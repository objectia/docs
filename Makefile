.PHONY: start build

start:
	yarn run start

build:
	yarn run build
