# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### client.usage.get()

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an API usage object, and returns an [error](/rest/#errors) otherwise.

The usage object has the following fields:

| Attribute | Type   | Description                                          |
| :-------- | :----- | :--------------------------------------------------- |
| allowed   | number | The number of allowed requests for the current month |
| used      | number | The number of used requests in the current month     |

#### Example

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  const usage = await client.usage.get()
  console.log('Number of used requests: ' + usage.used)
  console.log('Number of allowed requests: ' + usage.allowed)
} catch (err) {
  console.log('Failed to get API usage')
}
```
