# Products API

You can use the Products API to access and manipulate a store's catalog.

## Get product

### client.products.get(id)

Retrieves the product for the given id.

#### Arguments

| Argument | Type   | Description                   | Required |
| :------- | :----- | :---------------------------- | :------- |
| id       | string | ID of the product to look up. | Yes      |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  const product = await client.products.get('1')
  console.log('Product name: ' + product.name)
} catch (err) {
  console.log('Failed to retrieve product')
}
```

## Add product

### client.products.create(options)

Adds a new product to your catalog.

#### Arguments

| Argument    | Type   | Description          | Required |
| :---------- | :----- | :------------------- | :------- |
| name        | string | Product name.        | Yes      |
| description | string | Product description. | Yes      |
| price       | number | Product price        | Yes      |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  const options = {
    name: 'New widget',
    description: 'This is the widget description',
    price: 9.99,
  }
  const product = await client.products.create(options)
} catch (err) {
  console.log('Failed to add product')
}
```

## Update product

### client.products.update(id, options)

Updates the product for the given id.

#### Arguments

| Argument    | Type   | Description                       | Required |
| :---------- | :----- | :-------------------------------- | :------- |
| id          | string | ID of the product to look update. | Yes      |
| name        | string | Product name.                     | No       |
| description | string | Product description.              | No       |
| price       | number | Product price                     | No       |

#### Returns

This call returns a nothing if successful, and returns an [error](/rest/#errors) otherwise.

### Example request

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  const options = {
    name: 'New widget',
  }
  const product = await client.products.update('1', options)
} catch (err) {
  console.log('Failed to update product')
}
```

## Remove product

### client.products.delete(id)

Deletes the product for the given id.

#### Arguments

| Argument | Type   | Description                  | Required |
| :------- | :----- | :--------------------------- | :------- |
| id       | string | ID of the product to delete. | Yes      |

#### Returns

This call returns a nothing if successful, and returns an [error](/rest/#errors) otherwise.

### Example request

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  await client.products.delete('1')
} catch (err) {
  console.log('Failed to delete product')
}
```
