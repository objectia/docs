# Introduction

<img src="/img/javascript.png" width="50"/>

This is the documentation for the Javascript client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-js).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Javascript client for Salesfly API requires node.js version 8 or later. To check the version of your node.js environment, type the following in the terminal window:

```bash
node --version
```

You should see something like:

```
v12.1.0
```

If you don't have Node.js installed, [head over to nodejs.org and download the appropriate installer for your system](https://nodejs.org). Once you've installed Node, return to your terminal and run the command above once again. If you don't see the installed node version, you may need to relaunch your terminal.

## Installation

The Javascript client for Salesfly API is published on [NPM](https://npmjs.org/package/salesfly), so you can install it with yarn

```bash
yarn add salesfly
```

or with npm

```bash
npm install salesfly --save
```

## Using

To use the client in you code you import in like this:

```js
const salesfly = require('salesfly')
```

## Authentication

After importing the library, you can instantiate the API client.

```js
const client = new salesfly({
  apiKey: 'YOUR-API-KEY',
})
```

::: danger IMPORTANT
We recommend that you load API keys from an environment variable. It is bad practice to hard code it like in the example above.
:::

## Calling the API

And finally, we can call the API:

```js
let location = await client.geoip.get('8.8.8.8')
console.log('Country: ' + location.country_name) // prints "United States"
console.log('Country code: ' + location.country_code) // and    "US"
```

The API is asynchronous (based on promises), so you must use the `await` keyword. Alternatively you can call it like this:

```js
client.geoip
  .get('8.8.8.8')
  .then((location) => {
    console.log('Country: ' + location.country_name) // prints "United States"
    console.log('Country code: ' + location.country_code) // and    "US"
  })
  .catch((err) => {
    // ...
  })
```

## Error handling

The example above is not complete. It will work as long as everything goes as planned, but we all know that is not always the case.
We have to make sure that errors are handled by using a try-catch statement:

```js
// If you enter an invalid IP address, you get an error
try {
  let location = await client.geoip.get('288.8.8.8')
  // ...
} catch (err) {
  if (err.code === 'err-invalid-ip') {
    // ...
  } else {
    // ...
  }
}
```

The API functions throws an error upon failure. The <code>err</code> object contains the following fields:

```json
{
  "status": 400,
  "success": false,
  "message": "The IP address not valid",
  "code": "err-invalid-ip"
}
```

You will find all the error codes in the [REST API documentation](/rest/#errors).
