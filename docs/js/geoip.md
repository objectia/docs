# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### client.geoip.get(ip, options)

Looks up geolocation for an IP address.

#### Arguments

This call takes one or two arguments. The options argument is optional.

| Parameter        | Type    | Description                                                                               | Required |
| :--------------- | :------ | :---------------------------------------------------------------------------------------- | :------- |
| ip               | string  | The IP address or domain name to look up.                                                 | Yes      |
| options.fields   | string  | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | set to true if you want to reverse lookup ip address to hostname                          | No       |
| options.security | boolean | set to true if you want additional security info                                          | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) object, and returns an [error](/rest/#errors) otherwise.

#### Example

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  let options = {
    fields: 'country_code',
    hostname: false,
    security: false,
  }
  let location = await client.geoip.get('8.8.8.8', options)
  console.log('Country code: ' + location.country_code)
} catch (err) {
  console.log('Failed to get geo location')
}
```

## Get location of multiple IP addresses

### client.geoip.getBulk(ipList, options)

Looks up geolocation for multiple IP addresses.

#### Arguments

This call takes one or two arguments. The options argument is optional.

| Parameter        | Type    | Description                                                                               | Required |
| :--------------- | :------ | :---------------------------------------------------------------------------------------- | :------- |
| ipList           | string  | A comma separated string with the IP addresses or domain names to look up.                | Yes      |
| options.fields   | string  | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | set to true if you want to reverse lookup ip address to hostname                          | No       |
| options.security | boolean | set to true if you want additional security info                                          | No       |

#### Returns

This call returns an array of [IP location](/rest/geoip.html#get-location-by-ip) objects, and returns an [error](/rest/#errors) otherwise.

#### Example

```js
const Salesfly = require("salesfly")

const client = new Salesfly({
	apiKey: "YOUR-API-KEY",
})

try {
	let locations = await client.geoip.getBulk("8.8.8.8,google.com")
	for (let i=0; i<locations.lengtlocation
		console.log("Country code: " + locations[i].country_code)
	}
} catch (err) {
	console.log("Failed to get geo location")
}
```

## Get location of caller

### client.geoip.getCurrent(options)

Gets the geolocation of caller's current IP address.

#### Arguments

This call takes an optional arguments.

| Parameter        | Type    | Description                                                                               | Required |
| :--------------- | :------ | :---------------------------------------------------------------------------------------- | :------- |
| options.fields   | string  | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | set to true if you want to reverse lookup ip address to hostname                          | No       |
| options.security | boolean | set to true if you want additional security info                                          | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) object, and returns an [error](/rest/#errors) otherwise.

#### Example

```js
const Salesfly = require('salesfly')

const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
})

try {
  let location = await client.geoip.getCurrent()
  console.log('Country code: ' + location.country_code)
} catch (err) {
  console.log('Failed to get geo location')
}
```
