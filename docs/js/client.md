# Client

## Initialization

### salesfly(options)

Creates a new API client.

#### Arguments

The options object take one or more attributes:

| Attribute | Type   | Description                                                                                                                                                                                                   | Required |
| :-------- | :----- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :------- |
| apiKey    | string | You API key                                                                                                                                                                                                   | Yes      |
| timeout   | number | Connection timeout in seconds (default is 30 seconds)                                                                                                                                                         |
| logger    | object | The client provides a logger facility, so you can attach your own logger. You can use the Console logger, or you can write your own custom logger (you must follow the same interface as the Console logger). |

#### Returns

This call returns the API client object.

#### Example

```js
const client = new Salesfly({
  apiKey: 'YOUR-API-KEY',
  logger: console,
})
```

## Get version

### client.getVersion()

Gets the API client version.

#### Arguments

None

#### Returns

This call returns the version number as a string.

#### Example

```js
v = client.GetVersion()
console.log('Client version: ' + v) // prints "1.0.0"
```

<!--
### Retry configuration

By default the client will retry each failed request four times. A failed request is usually a connection error, or a HTTP status code in the 500 series (except 501 Not Implemented).
The retry mechanism will perform an exponential wait based on the number of attempts and limited by the provided minimum and maximum durations. For example it will wait: 1s, 2s, 4s, 8s, and so on.

```js
client.RetryMax = 4						// Meaning one attempt + four retries
client.RetryWaitMin = 1 * time.Second
client.RetryWaitMax = 30 * time.Second
```
-->
