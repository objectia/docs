# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

## Create PDF

### func (client \*PDF) Create(options \*[PDFOptions](/go/reference.html#pdfoptions)) ([]byte, error)

This endpoint converts HTML to PDF.

#### Arguments

| Attribute | Type                                        | Description                   | Required |
| :-------- | :------------------------------------------ | :---------------------------- | :------- |
| options   | [PDFOptions](/go/reference.html#pdfoptions) | PDF document creation options | Yes      |

#### Returns

This call returns an array of bytes containing the PDF document and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
    "fmt"
    "io/ioutil"
    "os"
    "github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := os.Getenv("YOUR-API-KEY")
    client, err := salesfly.NewClient(apiKey, nil)
    options := salesfly.PDFOptions{
        DocumentURL: "https://example.com/invoice.html",
    }
    buffer, err := client.PDF.Create(&options)
    if err != nil {
        fmt.Println("Failed to create PDF: ", err)
        return
    }
    ioutil.WriteFile("document.pdf", buffer, 0644)
}
```
