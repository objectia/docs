# Client

## Initialization

### func NewClient(apiKey string, httpClient \*http.Client) (\*Client, error)

Creates a new API client.

#### Arguments

| Parameter  | Type   | Description                        | Required |
| :--------- | :----- | :--------------------------------- | :------- |
| apiKey     | string | Your API key                       | Yes      |
| httpClient | struct | Pointer to a struct of http.Client |

#### Returns

This call returns the API client object.

#### Example

```go
apiKey := "YOUR-API-KEY"
client, err := salesfly.NewClient(apiKey, nil)
```

You can also provide your own http client

```go
import "net/http"

httpClient = &http.Client{
	Timeout: 30 * time.Second,
}

client, err := salesfly.NewClient(apiKey, httpClient)
```

## Get version

### func (c \*Client) GetVersion() string

Gets the API client version

#### Arguments

None

#### Returns

This call returns the version number as a string.

#### Example

```go
v := client.GetVersion()
println("Client version:", v)   // prints "1.0.0"
```

## Retry configuration

By default the client will retry each failed request four times. A failed request is usually a connection error, or a HTTP status code in the 500 series (except 501 Not Implemented).
The retry mechanism will perform an exponential wait based on the number of attempts and limited by the provided minimum and maximum durations. For example it will wait: 1s, 2s, 4s, 8s, and so on.

```go
client.RetryMax = 4						// Meaning one attempt + four retries
client.RetryWaitMin = 1 * time.Second
client.RetryWaitMax = 30 * time.Second
```

## Log configuration

The client provides a Logger facility, so you can attach your own logger.

```go
import "log"

client.Logger = log.New(os.Stderr, "", log.LstdFlags)
```
