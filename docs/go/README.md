# Introduction

<img src="/img/go.png" width="75"/>

This is the documentation for the Go client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-go).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Go client for Salesfly API requires golang version 1.8 or later. To check the version of your golang environment, type the following in the terminal window:

```bash
go version
```

You should see something like:

```bash
go version go1.12.3 darwin/amd64
```

If you do not have go installed, [head over to golang.org and download the appropriate installer for your system](https://golang.org/). Once you've installed go, return to your terminal and run the command above once again. If you don't see the installed go version, you may need to relaunch your terminal.

## Installation

To install the client library, use <code>go get</code> like you normally do:

```bash
go get github.com/salesfly/salesfly-go
```

## Using

Import the library into your code as <code>salesfly</code>:

```go
import "github.com/salesfly/salesfly-go"
```

## Authentication

After importing the library, you can create an API client.

```go
apiKey := "YOUR-API-KEY"
client, err := salesfly.NewClient(apiKey, nil)
```

::: danger IMPORTANT
We recommend that you load API keys from an environment variable. It is bad practice to hard code it like in the example above.
:::

## Calling the API

Then we are ready to call the API:

```go
location, err := client.GeoLocation.Get("8.8.8.8", nil)

println("Country:", location.Country)           // prints "United States"
println("Country code:", location.CountryCode)  // and    "US"
```

## Error handling

Each client API function returns an error object with the result. The error is a pointer to an [salesfly.Error](/go/reference.html#error) struct.

You handle the error in traditional golang style:

```go
// If you enter an invalid IP address, you get an error
location, err := client.GeoLocation.Get("288.8.8.8", nil)
if err != nil {
	e, ok := err.(*salesfly.Error)
	if ok {
		if e.Code == "err-invalid-ip" {
			// handle API error ...
		}
	} else {
		// other error
	}
}
```

You will find all the error codes in the [REST API documentation](/rest/#errors).
