# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail

### func (client \*Mail) Send(message \*[MailMessage](/go/reference.html#mailmessage)) (\*[MailReceipt](/go/reference.html#mailreceipt), error) {

Sends a mail message to one or more recipients.

#### Arguments

A [MailMessage](/go/reference.html#mailmessage) object can contain the following attributes:

| Attribute                 | Type      | Description                                                                                                | Required |
| :------------------------ | :-------- | :--------------------------------------------------------------------------------------------------------- | :------- |
| From                      | string    | The email address of the sender                                                                            | Yes      |
| To                        | []string  | A list of recipient email addresses                                                                        | Yes      |
| Subject                   | string    | The message subject                                                                                        | Yes      |
| Text                      | string    | The plain text message text                                                                                | Yes      |
| HTML                      | string    | The HTML message text                                                                                      |
| FromName                  | string    | The name of sender                                                                                         |
| Date                      | time.Time | The time when message was sent                                                                             |
| ReplyTo                   | string    | The email address for replies                                                                              |
| Cc                        | []string  | A list of cc email addresses                                                                               |
| Bcc                       | []string  | A list of bcc email addresses                                                                              |
| Attachments               | []string  | A list of file attachments (full path)                                                                     |
| Tags                      | []string  | A list of tags                                                                                             |
| Charset                   | string    | The charset (default: "UTF-8")                                                                             |
| Encoding                  | string    | The encoding (default: "quoted-printable"). Other values: "base64", "8bit"                                 |
| RequireTLS \*             | null.bool | Whether to require TLS when connecting to the destination host                                             |
| VerifyCertificate \*      | null.bool | Whether to verify the SSL certificate of the remote host when connecting                                   |
| OpenTracking \*           | null.bool | A flag that enables or disables open tracking                                                              |
| ClickTracking             | null.bool | A flag that enables or disables click tracking (html message only)                                         |
| PlainTextClickTracking \* | null.bool | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| UnsubscribeTracking \*    | null.bool | A flag that specifies if an unsubscribe link should be added                                               |
| TestMode \*               | null.bool | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a [MailReceipt](/go/reference.html#mailreceipt) object if the message was successfully processed and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
	"fmt"
	"github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := "YOUR-API-KEY"
	client, err := salesfly.NewClient(apiKey, nil)

	m := salesfly.NewMessage("me@example.com", "Test", "This is just a test", "you@example.com")
	m.AddAttachment("/Users/me/picture.png")
	receipt, err := client.Mail.Send(m)
	if err != nil {
		panic("Failed to send mail")
	}
	fmt.Println("Mail message ID:", receipt.ID)
}
```
