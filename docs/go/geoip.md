# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### func (c \*GeoLocation) Get(ip string, options \*[GeoLocationOptions](/go/reference.html#geolocationoptions)) (\*[IPLocation](/go/reference.html#iplocation), error)

Looks up geolocation for an IP address.

#### Arguments

| Parameter | Type   | Description                                                                        | Required |
| :-------- | :----- | :--------------------------------------------------------------------------------- | :------- |
| ip        | string | The IP address or domain name to look up.                                          | Yes      |
| options   | struct | Pointer to a struct of [GeoLocationOptions](/go/reference.html#geolocationoptions) | No       |

#### Returns

This call returns an [IPLocation](/go/reference.html#iplocation) object and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
	"fmt"
	"github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := "YOUR-API-KEY"
	client, err := salesfly.NewClient(apiKey, nil)

	options := &salesfly.GeoLocationOptions{
		DisplayFields: "country_code", // Return only country code
	}

	result, err := client.GeoLocation.Get("8.8.8.8", options)
	if err != nil {
		panic("Failed to get IP location")
	}
	fmt.Println("Country code:", result.CountryCode)
}
```

## Get location of multiple IP addresses

### func (c \*GeoLocation) GetBulk(iplist []string, options \*[GeoLocationOptions](/go/reference.html#geolocationoptions)) ([][iplocation](/go/reference.html#iplocation), error)

Looks up geolocation for multiple IP addresses.

#### Arguments

| Parameter | Type     | Description                                                                        | Required |
| :-------- | :------- | :--------------------------------------------------------------------------------- | :------- |
| iplist    | []string | The IP addresses or domain names to look up.                                       | Yes      |
| options   | struct   | Pointer to a struct of [GeoLocationOptions](/go/reference.html#geolocationoptions) | No       |

#### Returns

This call returns an array of [IPLocation](/go/reference.html#iplocation) objects and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
	"fmt"
	"github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := "YOUR-API-KEY"
	client, err := salesfly.NewClient(apiKey, nil)

	result, err := client.GeoLocation.GetBulk([]string{"1.2.3.4", "8.8.8.8"}, nil)
	if err != nil {
		panic("Failed to get IP location")
	}

	for _, v := range result {
		fmt.Printf("Location: %+v\n", v)
	}
}
```

## Get location of caller

### func (c \*GeoLocation) GetCurrent(options \*[GeoLocationOptions](/go/reference.html#geolocationoptions)) (\*[IPLocation](/go/reference.html#iplocation), error)

Get geolocation of caller's current IP address.

#### Arguments

| Parameter | Type   | Description                                                                        | Required |
| :-------- | :----- | :--------------------------------------------------------------------------------- | :------- |
| options   | struct | Pointer to a struct of [GeoLocationOptions](/go/reference.html#geolocationoptions) | No       |

#### Returns

This call returns an [IPLocation](/go/reference.html#iplocation) object and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
	"fmt"
	"github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := "YOUR-API-KEY"
	client, err := salesfly.NewClient(apiKey, nil)

	result, err := client.GeoLocation.GetCurrent(nil)
	if err != nil {
		panic("Failed to get IP location")
	}
	fmt.Printf("Location: %+v\n", result)
}
```
