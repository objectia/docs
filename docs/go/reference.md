# Models

## APIUsage

```go
type APIUsage struct {
	Allowed int	   // Number of allowed requests
	Used    int	   // Number of used requests
}
```

## Error

```go
type Error struct {
	Status  int      // HTTP status code
	Success bool     // Success flag
	Message string   // Descriptive error message
	Code    string   // Error code
}
```

You will find all the error codes in the [REST API documentation](/rest/#errors).

## GeoLocationOptions

```go
type GeoLocationOptions struct {
	DisplayFields    string     // comma separated list of fields to display (snake_case), rest of fields will be 0 or empty
	LookupHostname   bool       // set to true if you want to reverse lookup ip address to hostname
	ShowSecurityInfo bool       // set to true if you want additional security info
}
```

## IPCurrency

```go
type IPCurrency struct {
	Code          string
	NumericCode   string
	Name          string
	PluralName    string
	Symbol        string
	NativeSymbol  string
	DecimalDigits int
}
```

## IPLanguage

```go
type IPLanguage struct {
	Code       string
	Code2      string
	Name       string
	NativeName string
	RTL        bool
}
```

## IPLocation

```go
type IPLocation struct {
	IPAddress       string // ip address
	Type            string // ipv4 or ipv6
	Hostname        string // resolved host name if LookupHostname set to true in GeoLocationOptions
	Continent       string
	ContinentCode   string
	Country         string
	CountryNative   string
	CountryCode     string
	CountryCode3    string
	Capital         string
	Region          string
	RegionCode      string
	City            string
	Postcode        string
	Latitude        float64
	Longitude       float64
	PhonePrefix     string
	Currencies      []IPCurrency
	Languages       []IPLanguage
	Flag            string
	FlagEmoji       string
	IsEU            *bool
	TLD             string
	ISP             string
	Timezone        *IPTimezone
	Security        *IPSecurity
}
```

## IPSecurity

```go
type IPSecurity struct {
	IsProxy     bool
	ProxyType   string
	IsCrawler   bool
	CrawlerName string
	CrawlerType string
	IsTOR       bool
	ThreatLevel string
	ThreatTypes []string
}
```

## IPTimezone

```go
type IPTimezone struct {
	ID             string
	LocalTime      string
	GMTOffset      int
	Code           string
	DaylightSaving bool
}
```

## MailMessage

```go
import (
	"time"
	"gopkg.in/guregu/null.v3"
)

type MailMessage struct {
	Date        time.Time
	From        string
	FromName    string
	ReplyTo     string
	To          []string
	Cc          []string
	Bcc         []string
	Subject     string
	Text        string
	HTML        string
	Attachments []string
	Tags        []string
	Charset     string
	Encoding    string

	// Options:
	RequireTLS             null.Bool
	VerifyCertificate      null.Bool
	OpenTracking           null.Bool
	ClickTracking          null.Bool
	PlainTextClickTracking null.Bool
	UnsubscribeTracking    null.Bool
	TestMode               null.Bool
}
```

## MailReceipt

```go
type MailReceipt struct {
	ID                 string
	AcceptedRecipients int
	RejectedRecipients int
}
```

## PDFOptions

```go
type PDFOptions struct {
	DocumentURL       string
	DocumentHTML      string
	DocumentName      string
	PageFormat        string
	PageWidth         string
	PageHeight        string
	MarginTop         string
	MarginBottom      string
	MarginLeft        string
	MarginRight       string
	PrintBackground   bool
	HeaderText        string
	HeaderAlign       string
	HeaderMargin      int
	HeaderURL         string
	HeaderHTML        string
	FooterText        string
	FooterAlign       string
	FooterMargin      int
	FooterURL         string
	FooterHTML        string
	Orientation       string
	PageRanges        string
	PreferCSSPageSize bool
	Scale             float64
	Author            string
	Title             string
	Creator           string
	Subject           string
	Keywords          []string
	Language          string
	WatermarkURL      string
	WatermarkPosition string
	WatermarkOffsetX  int
	WatermarkOffsetY  int
	Encryption        string
	OwnerPassword     string
	UserPassword      string
	Permissions       string
}
```
