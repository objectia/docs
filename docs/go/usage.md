# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### func (client \*Usage) Get() (\*[APIUsage](/go/reference.html#apiusage), error)

Retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an [APIUsage](/go/reference.html#apiusage) object and an [Error](/go/reference.html#error) object.

#### Example

```go
package main

import (
	"fmt"
	"github.com/salesfly/salesfly-go"
)

func main() {
    apiKey := "YOUR-API-KEY"
	client, err := salesfly.NewClient(apiKey, nil)

	var result *salesfly.APIUsage
	result, err = client.Usage.Get()
	if err != nil {
		panic("Failed to get API usage")
	}
	fmt.Println("Number of allowed requests:", result.Allowed)
	fmt.Println("Number of used requests:", result.Used)
}
```
