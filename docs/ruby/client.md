# Client

## Initialize

### Client(options={})

Creates a new API client.

#### Arguments

| Parameter       | Type   | Description                                           | Required |
| :-------------- | :----- | :---------------------------------------------------- | :------- |
| options.api_key | string | Your API key                                          | Yes      |
| options.timeout | number | Connection timeout in seconds (default is 30 seconds) | No       |

#### Returns

This call returns the API client object.

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
    timeout: 30,
})
```

## Get client version

### client.version()

Gets the API client version

#### Arguments

None

#### Returns

This call returns the version number as a string.

#### Example

```ruby
v = client.version()
print v   # prints "1.0.0"
```
