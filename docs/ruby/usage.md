# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### client.usage.get()

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an API usage object, and throws an [APIError](/ruby/errors.html#apierror) otherwise.

The usage object has the following fields:

| Attribute | Type   | Description                                          |
| :-------- | :----- | :--------------------------------------------------- |
| allowed   | number | The number of allowed requests for the current month |
| used      | number | The number of used requests in the current month     |

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})

usage = client.usage.get()
print "Number of used requests: ", usage.used
print "Number of allowed requests: ", usage.allowed
```
