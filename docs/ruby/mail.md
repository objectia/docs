# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail

### client.mail.send(message)

Sends a mail message to one or more recipients.

#### Arguments

A message object can contain the following attributes:

| Attribute               | Type         | Description                                                                                                | Required |
| :---------------------- | :----------- | :--------------------------------------------------------------------------------------------------------- | :------- |
| from                    | string       | The email address of the sender                                                                            | Yes      |
| to                      | string array | A list of recipient email addresses                                                                        | Yes      |
| subject                 | string       | The message subject                                                                                        | Yes      |
| text                    | string       | The plain text message text                                                                                | Yes      |
| html                    | string       | The HTML message text                                                                                      |
| from_name               | string       | The name of sender                                                                                         |
| date                    | date         | The time when message was sent                                                                             |
| reply_to                | string       | The email address for replies                                                                              |
| cc                      | string array | A list of cc email addresses                                                                               |
| bcc                     | string array | A list of bcc email addresses                                                                              |
| attachments             | string array | A list of file attachments (full path)                                                                     |
| tags                    | string array | A list of tags                                                                                             |
| charset                 | string       | The charset (default: "UTF-8")                                                                             |
| encoding                | string       | The encoding (default: "quoted-printable"). Other values: "base64", "8bit"                                 |
| require_tls \*          | boolean      | Whether to require TLS when connecting to the destination host                                             |
| verify_cert \*          | boolean      | Whether to verify the SSL certificate of the remote host when connecting                                   |
| open_tracking \*        | boolean      | A flag that enables or disables open tracking                                                              |
| click_tracking \*       | boolean      | A flag that enables or disables click tracking (html message only)                                         |
| text_click_tracking \*  | boolean      | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| unsubscribe_tracking \* | boolean      | A flag that specifies if an unsubscribe link should be added                                               |
| test_mode \*            | boolean      | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a message receipt if the message was successfully processed, and throws an [APIError](/ruby/errors.html#apierror) otherwise.

The message receipt has the following fields:

| Attribute           | Type   | Description                       |
| :------------------ | :----- | :-------------------------------- |
| id                  | string | The message submit ID             |
| accepted_recipients | number | The number of accepted recipients |
| rejected_recipients | number | The number of rejected recipients |

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})

message = {
    "from" => "me@example.com",
    "to" => ["ok@example.com"],
    "subject" => "Test",
    "text" => "This is a test",
    "attachments" => ["/Me/pictures/photo.jpg"]
}
begin
    receipt = client.mail.send(message)
    print "Accepted recipients: ", receipt["accepted_recipients"]
rescue Salesfly::ResponseError => e
    # Handle error...
end
```
