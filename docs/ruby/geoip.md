# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### client.geoip.get(ip, options={})

Looks up geolocation for an IP address.

#### Arguments

| Parameter        | Type    | Description                                                                               | Required |
| :--------------- | :------ | :---------------------------------------------------------------------------------------- | :------- |
| ip               | string  | The IP address or domain name to look up.                                                 | Yes      |
| options.fields   | string  | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | set to true if you want to reverse lookup ip address to hostname                          | No       |
| options.security | boolean | set to true if you want additional security info                                          | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) dictionary, and throws an [APIError](/ruby/errors.html#apierror) otherwise.

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})

begin
    location = client.geoip.get("8.8.8.8")
    print "Country code: ", location["country_code"]
rescue Salesfly::ResponseError => e
    if e.code == "err-invalid-ip"
		# Handle error...
	else
		# Other error...
end
```

## Get location of multiple IP addresses

### client.geoip.get_bulk(ip_list, options={})

Looks up geolocation for multiple IP addresses.

#### Arguments

| Parameter        | Type    | Description                                                                                 | Required |
| :--------------- | :------ | :------------------------------------------------------------------------------------------ | :------- |
| ip_list          | list    | List of strings with the IP addresses or domain names to look up.                           | Yes      |
| options.fields   | string  | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | Set to true if you want to reverse lookup ip address to hostname                            | No       |
| options.security | boolean | Set to true if you want additional security info                                            | No       |

#### Returns

This call returns a list of [IP location](/rest/geoip.html#get-location-by-ip) dictionaries, and throws an [APIError](/ruby/errors.html#apierror) otherwise.

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})

begin
    locations = client.geoip.get_bulk(["8.8.8.8","google.com"])
    locations.each {|loc| puts "Country code: #{loc["country_code"]}" }
rescue Salesfly::ResponseError => e
    if e.code == "err-invalid-ip"
		# Handle error...
	else
		# Other error...
end
```

## Get location of caller

### client.geoip.get_current(options={})

Get geolocation of caller's current IP address.

#### Arguments

| Parameter        | Type    | Description                                                                                 | Required |
| :--------------- | :------ | :------------------------------------------------------------------------------------------ | :------- |
| options.fields   | string  | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | Set to true if you want to reverse lookup ip address to hostname                            | No       |
| options.security | boolean | Set to true if you want additional security info                                            | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) dictionary, and throws an [APIError](/ruby/errors.html#apierror) otherwise.

#### Example

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})

begin
    location = client.geoip.get_current()
    print "Country code: ", location["country_code"]
rescue Salesfly::ResponseError => e
    if e.code == "err-invalid-ip"
		# Handle error...
	else
		# Other error...
end
```
