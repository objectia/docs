# Errors

## APIError

```ruby
class APIError < StandardError
```

## APIConnectionError

```ruby
class APIConnectionError < APIError
```

## APITimeoutError

```ruby
class APITimeoutError < APIError
```

## ResponseError

```ruby
class ResponseError < APIError
    attr_reader :status, :code
    def initialize(msg="", status=400, code="")
end
```

