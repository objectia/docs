# Introduction

<img src="/img/ruby.svg" width="45"/>

This is the documentation for the Ruby client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-ruby).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Ruby client for Salesfly API requires Ruby version 2.2 or later. If you’re using a Mac or Linux machine, you probably already have Ruby installed. You can check this by opening up a terminal and running the following command:

```bash
ruby --version
```

You should see something like:

```bash
$ ruby --version
ruby 2.5.4
```

Windows users can use [RubyInstaller](https://rubyinstaller.org/) to install Ruby.

## Installation

The easiest way to instal salesfly-ruby is from RubyGems.

```bash
gem install salesfly
```

Or, you can clone the source code for salesfly-ruby, and install the library from there.

::: danger IMPORTANT
If the command line gives you a long error message that says Permission Denied in the middle of it, try running the above commands with sudo: sudo gem install salesfly.
:::

## Using

Now that we have Ruby and salesfly-ruby installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```ruby
require "salesfly"

api_key = ENV["SALESFLY_APIKEY"]

client = Salesfly::Client.new({
    api_key: api_key,
})
location = client.geoip.get("8.8.8.8")
print "Country code: ", location["country_code"]
```

## Error handling

Handle errors with a rescue block:

```ruby
# If you enter an invalid IP address, you get an error
begin
    location = client.geoip.get("x")
rescue Salesfly::ResponseError => e
    if e.code == "err-invalid-ip"
		# Handle error...
	else
		# Other error...
end
```

You will find all the error codes in the [REST API documentation](/rest/#errors).
