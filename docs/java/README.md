# Introduction

<img src="/img/java.svg" width="60"/>

This is the documentation for the Java client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-java).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Java client for Salesfly API requires the Java Standard Edition (SE) Development Kit (JDK) to be installed. If you don't know if you have the JDK installed, run the following command to see what version you have:

```bash
java -version
```

You should see something like

```bash
java version 1.8.0_101
```

The Salesfly API client requires Java SE 8 or higher, which will appear as version number 1.8 or above when you run the above command.

If you have an older version of Java or no JDK at all, you'll need to install the JDK before going any further. Follow the directions for installing the Java SE Development Kit for your platform (Windows, Mac, Linux) from the [Java SE Download Page](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or from the [free alterative OpenJDK](https://openjdk.java.net/).

## Installation

The Java client for Salesfly API is published on [Maven Central Repository](https://search.maven.org/search?q=salesfly-java).

If you are using Maven, add this dependency to your project's POM:

```xml
<dependency>
  <groupId>com.salesfly</groupId>
  <artifactId>salesfly-java</artifactId>
  <version>1.0.0</version>
</dependency>
```

If you prefer Gradle, add this dependency to your project's build file:

```groovy
compile "com.salesfly:salesfly-java:1.0.0"
```

Or you can manually install the client using the following JARs:

- [Salesfly-java](https://github.com/salesfly/salesfly-java/releases/latest)
- [Google Gson](https://repo1.maven.org/maven2/com/google/code/gson/gson/2.8.6)
- [Apache HttpClient](https://repo1.maven.org/maven2/org/apache/httpcomponents/httpclient/4.5.10)

### [ProGuard](http://proguard.sourceforge.net/)

If you're planning on using ProGuard, make sure that you exclude the salesfly bindings. You can do this by adding the following to your `proguard.cfg` file:

```
-keep class com.salesfly.** { *; }
```

## Using

Now that we have Java and salesfly-java installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.GeoLocation;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.IPLocation;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            IPLocation location = GeoLocation.get("8.8.8.8");
            System.out.println("Country code: " + location.getCountryCode());
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        } catch (APIException e) {
            System.err.println("API error: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
```

## Error handling

The API client will throw an [APIException](/java/exceptions.html#apiexception) upon error, and you handle the errors in the normal way with a try-catch statement.
APIException has three subclasses: [APIConnectionException](/java/exceptions.html#apiconnectionexception), [APITimeoutException](/java/exceptions.html#apitimeoutexception) and [ResponseException](/java/exceptions.html#responseexception).

If you enter an invalid IP address, you will get an exception:

```java
try
{
    SalesflyClient.init(apiKey);
    IPLocation location = GeoLocation.get("x");
} catch (APIConnectionException e) {
    System.err.println("Failed to connect to API server");
} catch (APITimeoutException e) {
    System.err.println("Connection to API server timed out");
} catch (ResponseException e) {
    System.err.println("API request failed:");
    System.err.println("* Status: " + e.getStatus());
    System.err.println("* Code: " + e.getCode());
    System.err.println("* Message: " + e.getMessage());
} catch (IllegalArgumentException e) {
    System.err.println("Error: " + e.getMessage());
}
```

You should use the `getCode()` method to determine the reason for the error. The example above will print the following:

```
API request failed:
* Status: 400
* Code: err-invalid-ip
* Message: Invalid IP address
```

You can find all the error codes in the [REST API documentation](/rest/#errors).
