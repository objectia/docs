# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

## Create PDF

### public static byte[] create([PDFOptions](/java/models.html#pdfoptions) options)

This endpoint converts HTML to PDF.

#### Arguments

| Attribute | Type                                       | Description                   | Required |
| :-------- | :----------------------------------------- | :---------------------------- | :------- |
| options   | [PDFOptions](/java/models.html#pdfoptions) | PDF document creation options | Yes      |

#### Returns

This call returns an array of bytes containing the PDF document, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import java.io.FileOutputStream;
import com.salesfly.SalesflyClient;
import com.salesfly.api.PDF;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.PDFOptions;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            PDFOptions options = new PDFOptions();
            options.setDocumentURL("https://example.com/invoice.html");
            byte buffer[] = PDF.create(options);
            try (FileOutputStream fos = new FileOutputStream("document.pdf")) {
                fos.write(buffer);
            }
        } catch (ResponseException e) {
            System.err.println("Failed to create PDF: " + e.getMessage());
        }
    }
}
```
