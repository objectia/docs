# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail

### public static [MailReceipt](/java/models.html#mailreceipt) send([MailMessage](/java/models.html#mailmessage) message)

Sends a mail message to one or more recipients.

#### Arguments

A [MailMessage](/java/models.html#mailmessage) object can contain the following attributes:

| Attribute                 | Type          | Description                                                                                                | Required |
| :------------------------ | :------------ | :--------------------------------------------------------------------------------------------------------- | :------- |
| from                      | String        | The email address of the sender                                                                            | Yes      |
| to                        | String[]      | A list of recipient email addresses                                                                        | Yes      |
| subject                   | String        | The message subject                                                                                        | Yes      |
| text                      | String        | The plain text message text                                                                                | Yes      |
| html                      | String        | The HTML message text                                                                                      |
| fromName                  | String        | The name of sender                                                                                         |
| date                      | ZonedDateTime | The time when message was sent                                                                             |
| replyTo                   | String        | The email address for replies                                                                              |
| cc                        | String[]      | A list of cc email addresses                                                                               |
| bcc                       | String[]      | A list of bcc email addresses                                                                              |
| attachments               | String[]      | A list of file attachments (full path)                                                                     |
| tags                      | String[]      | A list of tags                                                                                             |
| charset                   | String        | The charset (default: "UTF-8")                                                                             |
| encoding                  | String        | The encoding (default: "quoted-printable"). Other values: "base64", "8bit"                                 |
| requireTLS \*             | Boolean       | Whether to require TLS when connecting to the destination host                                             |
| verifyCertificate \*      | Boolean       | Whether to verify the SSL certificate of the remote host when connecting                                   |
| openTracking \*           | Boolean       | A flag that enables or disables open tracking                                                              |
| clickTracking             | Boolean       | A flag that enables or disables click tracking (html message only)                                         |
| plainTextClickTracking \* | Boolean       | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| unsubscribeTracking \*    | Boolean       | A flag that specifies if an unsubscribe link should be added                                               |
| testMode \*               | Boolean       | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a [MailReceipt](/java/models.html#mailreceipt) object if the message was successfully processed, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.Mail;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.MailMessage;
import com.salesfly.models.MailReceipt;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            MailMessage message = new MailMessage("me@example.com", "Test", "This is a test", "you@example.com");
            message.addAttachment("/Users/me/picture.png");
            MailReceipt receipt = Mail.send(message);
            System.out.println("Mail submit ID: " + receipt.getSubmitID());
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        }
    }
}
```
