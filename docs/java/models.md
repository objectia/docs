# Models

## APIUsage

```java
package com.salesfly.models;

public class APIUsage {
    public int getGeoLocationRequests();
    public int getMailRequests();
}

```

## IPCurrency

```java
package com.salesfly.models;

public final class IPCurrency {
	public String getCode();
	public String getNumericCode();
	public String getName();
	public String getPluralName();
	public String getSymbol();
	public String getNativeSymbol();
	public int getDecimalDigits();
}
```

## IPLanguage

```java
package com.salesfly.models;

public final class IPLanguage {
    public String getCode();
    public String getCode2();
    public String getName();
    public String getNativeName();
    public Boolean getRtl();
    public Boolean isRtl();
}
```

## IPLocation

```java
package com.salesfly.models;

public class IPLocation {
    public String getIpAddress();
    public String getType();
    public String getHostname();
    public String getContinent();
    public String getContinentCode();
    public String getCountry();
    public String getCountryNative();
    public String getCountryCode();
    public String getCountryCode3();
    public String getCapital();
    public String getRegion();
    public String getRegionCode();
    public String getCity();
    public String getPostcode();
    public float getLatitude();
    public float getLongitude();
    public String getPhonePrefix();
    public IPCurrency[] getCurrencies();
    public IPLanguage[] getLanguages();
    public String getFlag();
    public String getFlagEmoji();
    public Boolean getIsEU();
    public Boolean isIsEU();
    public String getTld();
    public IPTimezone getTimezone();
    public IPSecurity getSecurity();
}
```

## IPSecurity

```java
package com.salesfly.models;

public final class IPSecurity {
    public Boolean isProxy();
    public String getProxyType();
    public Boolean isCrawler();
    public String getCrawlerName();
    public String getCrawlerType();
    public Boolean isTOR();
    public String getThreatLevel();
    public String[] getThreatTypes();
}
```

## IPTimezone

```java
package com.salesfly.models;

public final class IPTimezone {
	public String getId();
	public Date getLocalTime();
	public int getGmtOffset();
	public String getCode();
	public Boolean getDaylightSaving();
	public Boolean isDaylightSaving();
}
```

## MailMessage

```java
package com.salesfly.models;

public class MailMessage {
	public MailMessage(final String from, final String subject, final String text, final String ... to);
    public void setDate(final ZonedDateTime date);
	public ZonedDateTime getDate();
    public void setFrom(final String from);
	public String getFrom();
    public void setReplyTo(final String replyTo);
	public String getReplyTo();
	public String[] getTo();
    public void addCc(final String ...cc);
    public void addBcc(final String ...bcc);
	public String getSubject();
	public String getText();
    public void setHTML(final String html);
    public void addAttachment(final String fileName);
    public void addTag(final String tag);
    public void setEncoding(final String encoding);
	public String getEncoding();
    public void setCharset(final String charset);
    public String getCharset();
    public void setRequireTLS(final Boolean flag);
    public void setVerifyCertificate(final Boolean flag);
    public void setOpenTracking(final Boolean flag);
    public void setClickTracking(final Boolean flag);
    public void setPlainTextClickTracking(final Boolean flag);
    public void setUnsubscribeTracking(final Boolean flag);
    public void setTestMode(final Boolean flag);
}
```

## MailReceipt

```java
package com.salesfly.models;

public class MailReceipt {
    public String getSubmitID();
    public int getAcceptedRecipients();
    public int getRejectedRecipients();
}
```

## PDFOptions

```java
package com.salesfly.models;

public class PDFOptions {
    public void setDocumentURL(final String documentURL);
    public String getDocumentURL();

    public void setDocumentHTML(final String documentHTML);
    public String getDocumentHTML();

    public void setDocumentName(final String name);
    public String getDocumentName();

    public void setMarginTop(final String margin);
    public String getMarginTop();

    public void setMarginBottom(final String margin);
    public String getMarginBottom();

    public void setMarginLeft(final String margin);
    public String getMarginLeft();

    public void setMarginRight(final String margin);
    public String getMarginRight();

    public void setOrientation(final String orientation);
    public String getOrientation();

    public void setPageFormat(final String format);
    public String getPageFormat();

    public void setPageWidth(final String width);
    public String getPageWidth();

    public void setPageHeight(final String height);
    public String getPageHeight();

    public void setPageRanges(final String ranges);
    public String getPageRanges();

    public void setScale(final double scale);
    public double getScale();

    public void setHeaderText(final String text);
    public String getHeaderText();

    public void setHeaderAlign(final String alignment);
    public String getHeaderAlign();

    public void setHeaderAlign(final int margin);
    public int getHeaderMargin();

    public void setHeaderHTML(final String html);
    public String getHeaderHTML();

    public void setHeaderURL(final String url);
    public String getHeaderURL();

    public void setFooterText(final String text);
    public String getFooterText();

    public void setFooterAlign(final String alignment);
    public String getFooterAlign();

    public void setFooterAlign(final int margin);
    public int getFooterMargin();

    public void setFooterHTML(final String html);
    public String getFooterHTML();

    public void setFooterURL(final String url);
    public String getFooterURL();

    public void setPrintBackground(final boolean flag);
    public boolean getPrintBackground();

    public void setPreferCSSPageSize(final boolean flag);
    public boolean getPreferCSSPageSize();

    public void setWatermarkURL(final String url);
    public String getWatermarkURL();

    public void setWatermarkPosition(final String position);
    public String getWatermarkPosition();

    public void setWatermarkOffsetX(final int offset);
    public int getWatermarkOffsetX();

    public void setWatermarkOffsetY(final int offset);
    public int getWatermarkOffsetY();

    public void setTitle(final String title);
    public String getTitle();

    public void setAuthor(final String author);
    public String getAuthor();

    public void setCreator(final String creator);
    public String getCreator();

    public void setSubject(final String subject);
    public String getSubject();

    public void setKeywords(final String[] keywords);
    public String[] getKeywords();

    public void setLanguage(final String language);
    public String getLanguage();

    public void setEncryption(final String encryption);
    public String getEncryption();

    public void setOwnerPassword(final String password);
    public String getOwnerPassword();

    public void setUserPassword(final String password);
    public String getUserPassword();

    public void setPermissions(final String permissions);
    public String getPermissions();
}
```
