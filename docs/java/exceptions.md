# Exceptions

## APIException

```java
package com.salesfly.exceptions;

public abstract class APIException extends Exception {
    public APIException(String message);
}
```

## APIConnectionException

```java
package com.salesfly.exceptions;

public class APIConnectionException extends APIException {
    public APIConnectionException(String message);
}
```

## APITimeoutException

```java
package com.salesfly.exceptions;

public class APITimeoutException extends APIException {
    public APITimeoutException(String message);
}
```

## ResponseException

```java
package com.salesfly.exceptions;

public class ResponseException extends APIException {
    public ResponseException(int status, String message, String code);
    public int getStatus();
    public String getCode();
}
```
