# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### public static [APIUsage](/java/models.html#apiusage) get()

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an [APIUsage](/java/models.html#apiusage) object, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.Usage;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.APIUsage;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            APIUsage usage = Usage.get();
            System.out.println("Mail requests: " + usage.getMailRequests());
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        } catch (APIException e) {
            System.err.println("API error: " + e.getMessage());
        }
    }
}
```
