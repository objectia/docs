# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### public static [IPLocation](/java/models.html#iplocation) get(final String ip, final String fields, final Boolean hostname, final Boolean security)

Looks up geolocation for an IP address.

#### Arguments

| Argument | Type    | Description                                                | Required |
| :------- | :------ | :--------------------------------------------------------- | :------- |
| ip       | String  | The IP address or domain name to look up.                  | Yes      |
| fields   | String  | Comma separated list of fields to return to the caller.    | No       |
| hostname | Boolean | Look up host name from IP address.                         | No       |
| security | Boolean | Return extended security information about the IP address. | No       |

#### Returns

This call returns an [IPLocation](/java/models.html#iplocation) object, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.GeoLocation;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.IPLocation;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            IPLocation location = GeoLocation.get("8.8.8.8");
            System.out.println("Country code: " + location.getCountryCode());
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        } catch (APIException e) {
            System.err.println("API error: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
```

## Get location of multiple IP addresses

### public static List&lt;[IPLocation](/java/models.html#iplocation)&gt; getBulk(final String ipList[])

### public static List&lt;[IPLocation](/java/models.html#iplocation)&gt; getBulk(final String ipList[], final String fields, final Boolean hostname, final Boolean security)

Looks up geolocation for multiple IP addresses.

#### Arguments

| Argument | Type     | Description                                                        | Required |
| :------- | :------- | :----------------------------------------------------------------- | :------- |
| ipList   | String[] | Array of strings with the IP addresses or domain names to look up. | Yes      |
| fields   | String   | Comma separated list of fields to return to the caller.            | No       |
| hostname | Boolean  | Look up host name from IP address.                                 | No       |
| security | Boolean  | Return extended security information about the IP address.         | No       |

#### Returns

This call returns a list of [IPLocation](/java/models.html#iplocation) objects, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.GeoLocation;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.IPLocation;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            List<IPLocation> locations = GeoLocation.getBulk(new String[]{"8.8.8.8", "google.com"});
            for (IPLocation l : locations) {
                System.out.println("Country code: " + l.getCountryCode());
            }
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        } catch (APIException e) {
            System.err.println("API error: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
```

## Get location of caller

### public static [IPLocation](/java/models.html#iplocation) getCurrent()

### public static [IPLocation](/java/models.html#iplocation) getCurrent(final String fields, final Boolean hostname, final Boolean security)

Gets geolocation of caller's current IP address.

#### Arguments

| Argument | Type    | Description                                                | Required |
| :------- | :------ | :--------------------------------------------------------- | :------- |
| fields   | String  | Comma separated list of fields to return to the caller.    | No       |
| hostname | Boolean | Look up host name from IP address.                         | No       |
| security | Boolean | Return extended security information about the IP address. | No       |

#### Arguments

This call returns an [IPLocation](/java/models.html#iplocation) object, and throws an [APIException](/java/exceptions.html#apiexception) otherwise.

#### Example

```java
package examples;

import com.salesfly.SalesflyClient;
import com.salesfly.api.GeoLocation;
import com.salesfly.exceptions.APIException;
import com.salesfly.exceptions.ResponseException;
import com.salesfly.models.IPLocation;

public class Example {
    public static void main(String[] args) {
        String apiKey = System.getenv("SALESFLY_APIKEY");

        try {
            SalesflyClient.init(apiKey);
            IPLocation location = GeoLocation.getCurrent();
            System.out.println("Country code: " + location.getCountryCode());
        } catch (ResponseException e) {
            System.err.println("Response error: " + e.getMessage());
        } catch (APIException e) {
            System.err.println("API error: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
```
