# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### client.geoip.get(ip, fields=None, hostname=false, security=false)

Looks up geolocation for an IP address.

#### Arguments

| Parameter | Type   | Description                                                | Required |
| :-------- | :----- | :--------------------------------------------------------- | :------- |
| ip        | string | The IP address or domain name to look up.                  | Yes      |
| fields    | string | Comma separated list of fields to return to the caller.    | No       |
| hostname  | bool   | Look up host name from IP address                          | No       |
| security  | bool   | Return extended security information about the IP address. | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) dictionary, and throws an [APIError](/python/errors.html#apierror) otherwise.

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    location = client.geoip.get("8.8.8.8")
    print("Country code: {0}".format(location["country_code"]))
except:
    print("Failed to get geo location")
```

## Get location of multiple IP addresses

### client.geoip.get_bulk(ip_list, fields=None, hostname=false, security=false)

Looks up geolocation for multiple IP addresses.

#### Arguments

| Parameter | Type   | Description                                                       | Required |
| :-------- | :----- | :---------------------------------------------------------------- | :------- |
| ip_list   | list   | List of strings with the IP addresses or domain names to look up. | Yes      |
| fields    | string | Comma separated list of fields to return to the caller.           | No       |
| hostname  | bool   | Look up host name from IP address.                                | No       |
| security  | bool   | Return extended security information about the IP address.        | No       |

#### Returns

This call returns a list of [IP location](/rest/geoip.html#get-location-by-ip) dictionaries, and throws an [APIError](/python/errors.html#apierror) otherwise.

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    locations = client.geoip.get_bulk(["8.8.8.8,google.com"])
    for i in range(len(locations)):
        print("Country code: {0}".format(locations[i]["country_code"]))
except:
    print("Failed to get geo location")
```

## Get location of caller

### client.geoip.get_current(fields=None, hostname=false, security=false)

Gets geolocation of caller's current IP address.

#### Arguments

| Parameter | Type   | Description                                                | Required |
| :-------- | :----- | :--------------------------------------------------------- | :------- |
| fields    | string | Comma separated list of fields to return to the caller.    | Yes      |
| hostname  | bool   | Look up host name from IP address.                         | No       |
| security  | bool   | Return extended security information about the IP address. | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) dictionary, and throws an [APIError](/python/errors.html#apierror) otherwise.

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    location = client.geoip.get_current()
    print("Country code: {0}".format(location["country_code"]))
except:
    print("Failed to get geo location")
```
