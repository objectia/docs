# Introduction

<img src="/img/python.svg" width="60"/>

This is the documentation for the Python client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-python).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Python client for Salesfly API requires Python version 3.4 or later. If you’re using a Mac or Linux machine, you probably already have Python installed. You can check this by opening up a terminal and running the following command:

```bash
python --version
```

You should see something like:

```bash
Python 3.7.1
```

Windows users can follow this [tutorial for installing Python on Windows](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-local-programming-environment-on-windows-10), or follow the [instructions from Python's documentation](https://docs.python.org/3/using/windows.html).

## Installation

The easiest way to install the library is using [pip](http://www.pip-installer.org/en/latest/), a package manager for Python that makes it easier to install the libraries you need. Simply run this in the terminal:

```bash
pip install salesfly
```

If you get a <code>pip: command not found error</code>, you can also use <code>easy_install</code> by running this in your terminal:

```bash
easy_install salesfly
```

If you'd prefer a manual installation, you can [download the source code (ZIP)](https://github.com/salesfly/salesfly-python/zipball/master) for <code>salesfly-python</code> and then install the library by running:

```bash
python setup.py install
```

in the folder containing the salesfly-python library.

## Using

Now that we have Python and salesfly-python installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```python
import os
import salesfly

APIKEY = os.environ.get("SALESFLY_APIKEY")

client = salesfly.Client(api_key=APIKEY)
location = client.geoip.get("8.8.8.8")
print("Country code: {0}".format(location["country_code"]))
```

## Error handling

Handle errors with try...except:

```python
import salesfly
from salesfly.errors import ResponseError

# If you enter an invalid IP address, you get an error
try:
    client = salesfly.Client(api_key="YOUR-API-KEY")
    location = client.geoip.get("x")
except ValueError as e:
    # Missing API key?
except ResponseError as e:
	if e.code == "err-invalid-ip":
		# Handle error...
	else:
		# Other error...
```

You will find all the error codes in the [REST API documentation](/rest/#errors).
