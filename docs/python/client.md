# Client

## Initialize

### Client(api_key=None, timeout=30)

Create a new API client.

The client provides a Logger facility, so you can attach your own logger. Here we are using the Console API as logger. You can write your own custom
logger, but you have to follow the same interface as the Console logger.

#### Arguments

| Parameter | Type    | Description                                           | Required |
| :-------- | :------ | :---------------------------------------------------- | :------- |
| api_key   | string  | Your API key                                          | Yes      |
| timeout   | integer | Connection timeout in seconds (default is 30 seconds) | No       |

#### Returns

This call returns the API client object.

#### Example

```python
import os
import salesfly

APIKEY = os.environ.get("SALESFLY_APIKEY")

client = salesfly.Client(api_key=API_KEY, timeout=60)
```

## Get client version

### client.version()

Gets the API client version

#### Arguments

None

#### Returns

This call returns the version number as a string.

#### Example

```python
v = client.version()
print("Client version: {0}".format(v))   # prints "1.0.0"
```

<!--
### Retry configuration

By default the client will retry each failed request four times. A failed request is usually a connection error, or a HTTP status code in the 500 series (except 501 Not Implemented).
The retry mechanism will perform an exponential wait based on the number of attempts and limited by the provided minimum and maximum durations. For example it will wait: 1s, 2s, 4s, 8s, and so on.

```js
client.RetryMax = 4						// Meaning one attempt + four retries
client.RetryWaitMin = 1 * time.Second
client.RetryWaitMax = 30 * time.Second
```
-->
