# Errors

## APIError

```python
class APIError(Exception):
    pass
```

## APIConnectionError

```python
class APIConnectionError(APIError):
    pass
```

## APITimeoutError

```python
class APITimeoutError(APIError):
    pass
```

## ResponseError

```python
class ResponseError(APIError):
    def __init__(self, status, msg="", code="")
```

