module.exports = {
  title: 'Salesfly API Documentation',
  description: 'API Documentation',
  base: '/',
  port: 3005,
  dest: 'public',
  plugins: ['@vuepress/nprogress'],
  head: [
    [
      'link',
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        type: 'image/png',
        href: '/favicons/apple-icon-180x180.png',
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicons/favicon-32x32.png',
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicons/favicon-16x16.png',
      },
    ],
    ['link', { rel: 'manifest', href: '/favicons/manifest.json' }],
    [
      'link',
      {
        rel: 'shortcut icon',
        type: 'image/x-icon',
        href: '/favicons/favicon.ico',
      },
    ],
    [
      'link',
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/css/uikit.css',
      },
    ],
    [
      'script',
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js',
      },
    ],
    [
      'script',
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit-icons.min.js',
      },
    ],
  ],
  markdown: {
    lineNumbers: false,
  },
  themeConfig: {
    logo: '/img/logo.png',
    nav: [
      {
        text: 'Clients',
        ariaLabel: 'Clients Menu',
        items: [
          { text: 'C#/.NET', link: '/csharp/' },
          { text: 'Go', link: '/go/' },
          { text: 'Java', link: '/java/' },
          { text: 'Javascript', link: '/js/' },
          { text: 'PHP', link: '/php/' },
          { text: 'Python', link: '/python/' },
          { text: 'Ruby', link: '/ruby/' },
          { text: 'Swift', link: '/swift/' },
        ],
      },
      { text: 'REST API', link: '/rest/' },
      { text: 'Home', link: 'https://salesfly.com' },
      { text: 'Support', link: 'https://help.salesfly.com' },
      { text: 'GitHub', link: 'https://github.com/salesfly' },
    ],
    sidebar: {
      '/csharp/': [
        {
          title: 'C#/.NET client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/csharp/client', 'API Client'],
            ['/csharp/geoip', 'Geolocation API'],
            //['/csharp/mail', 'Mail API'],
            ['/csharp/pdf', 'PDF API'],
            ['/csharp/usage', 'Usage API'],
            ['/csharp/models', 'Models'],
            ['/csharp/exceptions', 'Exceptions'],
          ],
        },
      ],
      '/go/': [
        {
          title: 'Go client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/go/client', 'API Client'],
            ['/go/geoip', 'Geolocation API'],
            //['/go/mail', 'Mail API'],
            ['/go/pdf', 'PDF API'],
            ['/go/usage', 'Usage API'],
            ['/go/reference', 'Reference'],
          ],
        },
      ],
      '/java/': [
        {
          title: 'Java client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/java/client', 'API Client'],
            ['/java/geoip', 'Geolocation API'],
            // ['/java/mail', 'Mail API'],
            ['/java/pdf', 'PDF API'],
            ['/java/usage', 'Usage API'],
            ['/java/models', 'Models'],
            ['/java/exceptions', 'Exceptions'],
          ],
        },
      ],
      '/js/': [
        {
          title: 'Javascript client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/js/client', 'API Client'],
            ['/js/geoip', 'Geolocation API'],
            //   ['/js/mail', 'Mail API'],
            ['/js/pdf', 'PDF API'],
            ['/js/products', 'Products API'],
            ['/js/usage', 'Usage API'],
            // ['/js/reference', 'Reference'],
          ],
        },
      ],
      '/php/': [
        {
          title: 'PHP client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/php/client', 'API Client'],
            ['/php/geoip', 'Geolocation API'],
            //  ['/php/mail', 'Mail API'],
            ['/php/pdf', 'PDF API'],
            ['/php/usage', 'Usage API'],
            ['/php/exceptions', 'Exceptions'],
          ],
        },
      ],
      '/python/': [
        {
          title: 'Python client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/python/client', 'API Client'],
            ['/python/geoip', 'Geolocation API'],
            //   ['/python/mail', 'Mail API'],
            ['/python/pdf', 'PDF API'],
            ['/python/usage', 'Usage API'],
            ['/python/errors', 'Errors'],
          ],
        },
      ],
      '/ruby/': [
        {
          title: 'Ruby client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/ruby/client', 'API Client'],
            ['/ruby/geoip', 'Geolocation API'],
            //  ['/ruby/mail', 'Mail API'],
            ['/ruby/pdf', 'PDF API'],
            ['/ruby/usage', 'Usage API'],
            ['/ruby/errors', 'Errors'],
          ],
        },
      ],
      '/swift/': [
        {
          title: 'Swift client for Salesfly API',
          collapsable: false,
          children: [
            '',
            ['/swift/client', 'API Client'],
            ['/swift/geoip', 'Geolocation API'],
            //    ['/swift/mail', 'Mail API'],
            ['/swift/usage', 'Usage API'],
            ['/swift/pdf', 'PDF API'],
            ['/swift/models', 'Models'],
            ['/swift/errors', 'Errors'],
          ],
        },
      ],
      '/rest/': [
        {
          title: 'REST API',
          collapsable: false,
          children: [
            '',
            '/rest/currency',
            '/rest/geoip',
            //   '/rest/mail',
            '/rest/pdf',
            '/rest/products',
            //    '/rest/sms',
            '/rest/usage',
          ],
        },
      ],
    },
  },
}
