# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### public static func get() -> [APIUsage](/swift/models.html#apiusage)?

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an [APIUsage](/swift/models.html#apiusage) object, and throws an [SalesflyError](/swift/errors.html#apiexception) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    let usage = try Usage.get()
    print("Allowed requests:", usage!.allowed)
    print("Used requests:", usage!.used)
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print(error)
}
```
