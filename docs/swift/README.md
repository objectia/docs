# Introduction

<img src="/img/swift.png" width="50"/>

This is the documentation for the Swift client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-swift).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The Swift client for Salesfly API requires the Swift 4.0 or later to be installed. If you’re using a Mac and have Xcode 10.0 or later installed, you already have the necessary Swift tools installed. You can check the version of your Swift compiler by opening up a terminal and running the following command:

```bash
swift --version
```

You should see something like:

```
Apple Swift version 4.1.2
```

## Installation

There are many ways of installing the Swift API client:

### Swift Package Manager

To integrate using Apple's Swift package manager, add the following as a dependency to your Package.swift:

```swift
.package(url: "https://github.com/salesfly/salesfly-swift.git", .upToNextMajor(from: "1.0.0"))
```

and then specify "Salesfly" as a dependency of the Target in which you wish to use Salesfly. Here's an example

```swift
// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "MyPackage",
    products: [
        .library(
            name: "MyPackage",
            targets: ["MyPackage"]),
    ],
    dependencies: [
        .package(url: "https://github.com/salesfly/salesfly-swift.git", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .target(
            name: "MyPackage",
            dependencies: ["Salesfly"])
    ]
)
```

Then run <code>swift package update</code> to install or update the dependencies.

### Accio

Accio is a dependency manager based on SwiftPM which can build frameworks for iOS/macOS/tvOS/watchOS. Therefore the integration steps of Salesfly are exactly the same as described above. Once your Package.swift file is configured, run <code>accio update</code> instead of <code>swift package update</code>.

### CocoaPods

To use CocoaPods, add the following entry in your Podfile:

```
pod 'Salesfly', '~> 1.0'
```

Then run <code>pod install</code>.

In any file you'd like to use Salesfly in, don't forget to import the framework with <code>import Salesfly</code>.

### Carthage

To use Carthage, make the following entry in your Cartfile:

```
github "salesfly/salesfly-swift" ~> 1.0
```

Then run <code>carthage update</code>.

## Using

Now that we have Swift and salesfly-swift installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    let location = try GeoLocation.get(ip: "8.8.8.8")
    print("Country code:", location!.countryCode!)
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print(error)
}
```

## Error handling

The API client will throw an [SalesflyError](/swift/errors.html#salesflyerror) upon error, and you handle the errors in the normal way with a do-catch statement.

If you enter an invalid IP address, you will get an exception:

```swift
do {
    try SalesflyClient.initialize(apiKey: apiKey)
    let location = try GeoLocation.get(ip: "x")
    // ...
} catch let err as SalesflyError {
    print("API request failed:")
    switch err {
        case .badRequest(let params):
            print("* Status:", 400)
            print("* Code:", params.code)
            print("* Message:", params.reason)
        default:
            print(err)
    }
} catch {
    print(error)
}
```

You should use the `code` attribute to determine the reason for the error. The example above will print the following:

```
API request failed:
* Status: 400
* Code: err-invalid-ip
* Message: Invalid IP address
```

You can find all the error codes in the [REST API documentation](/rest/#errors).
