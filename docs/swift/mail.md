# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail

### public static func send(message: [MailMessage](/swift/models.html#mailmessage)) -> [MailReceipt](/swift/models.html#mailreceipt)?

Sends a mail message to one or more recipients.

#### Arguments

A [MailMessage](/swift/models.html#mailmessage) object can contain the following attributes:

| Attribute                 | Type     | Description                                                                                                | Required |
| :------------------------ | :------- | :--------------------------------------------------------------------------------------------------------- | :------- |
| from                      | String   | The email address of the sender                                                                            | Yes      |
| to                        | [String] | An array of recipient email addresses                                                                      | Yes      |
| subject                   | String   | The message subject                                                                                        | Yes      |
| text                      | String   | The plain text message text                                                                                | Yes      |
| html                      | String?  | The HTML message text                                                                                      |
| fromName                  | String?  | The name of sender                                                                                         |
| date                      | Date?    | The time when message was sent                                                                             |
| replyTo                   | String?  | The email address for replies                                                                              |
| cc                        | [String] | An array of cc email addresses                                                                             |
| bcc                       | [String] | An array of bcc email addresses                                                                            |
| attachments               | [String] | An array of file attachments (full path)                                                                   |
| tags                      | [String] | An array of tags                                                                                           |
| charset                   | String?  | The charset (default: "UTF-8")                                                                             |
| encoding                  | String?  | The encoding (default: "quoted-printable"). Other values: "base64", "8bit"                                 |
| requireTLS \*             | Bool?    | Whether to require TLS when connecting to the destination host                                             |
| verifyCertificate \*      | Bool?    | Whether to verify the SSL certificate of the remote host when connecting                                   |
| openTracking \*           | Bool?    | A flag that enables or disables open tracking                                                              |
| clickTracking             | Bool?    | A flag that enables or disables click tracking (html message only)                                         |
| plainTextClickTracking \* | Bool?    | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| unsubscribeTracking \*    | Bool?    | A flag that specifies if an unsubscribe link should be added                                               |
| testMode \*               | Bool?    | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a [MailReceipt](/swift/models.html#mailreceipt) object if the message was successfully processed, and throws an [SalesflyError](/swift/errors.html#salesflyerror) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    var message = MailMessage(from: "me@example.com", to: ["you@example.com"], subject: "Test", text: "This is just a test")
    message.attachments = ["/Me/pictures/photo.jpg"]
    let receipt = try Mail.send(message: message)
    print("Accepted recipients:", receipt!.acceptedRecipients)
} catch let err as SalesflyError {
    print("API request failed:", err)
} catch {
    print("Error:", error)
}
```
