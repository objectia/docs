# Client

## Initialization

### public static initialize(apiKey: String, timeout: TimeInterval)

Initializes the API client.

#### Arguments

| Parameter | Type         | Description                                           | Required |
| :-------- | :----------- | :---------------------------------------------------- | :------- |
| apiKey    | String       | Your API key                                          | Yes      |
| timeout   | TimeInterval | Connection timeout in seconds (default is 30 seconds) | No       |

#### Returns

This call does not return anything, but throws an [SalesflyError](/swift/errors.html#salesflyerror) if it fails.

#### Example

```swift
let apiKey = "YOUR-API-KEY"
do {
    try SalesflyClient.initialize(apiKey: apiKey)
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print("Other error...")
}
```

## Version

### public static getVersion() -> String

Gets the API client version.

#### Arguments

None

#### Returns

The API client version string.

#### Example

```swift
let version = SalesflyClient.getVersion()
print(version)
```

This will print something like this on the screen:

```
Client version: 1.0.0
```
