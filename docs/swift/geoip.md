# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### public static func get(ip: String, fields: String?, hostname: Bool, security: Bool) -> [IPLocation](/swift/models.html#iplocation)?

Looks up geolocation for an IP address.

#### Arguments

| Argument | Type    | Description                                                | Required |
| :------- | :------ | :--------------------------------------------------------- | :------- |
| ip       | String  | The IP address or domain name to look up.                  | Yes      |
| fields   | String? | Comma separated list of fields to return to the caller.    | No       |
| hostname | Bool    | Look up host name from IP address.                         | No       |
| security | Bool    | Return extended security information about the IP address. | No       |

#### Returns

This call returns an [IPLocation](/swift/models.html#iplocation) object, and throws an [SalesflyError](/swift/errors.html#salesflyerror) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    let location = try GeoLocation.get(ip: "8.8.8.8")
    print("Country code:", location!.countryCode!)
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print("Error:", error)
}
```

## Get location of multiple IP addresses

### public static func getBulk(ipList: [String], fields: String?, hostname: Bool, security: Bool) -> [[IPLocation](/swift/models.html#iplocation)]?

Looks up geolocation for multiple IP addresses.

#### Arguments

| Argument | Type     | Description                                                        | Required |
| :------- | :------- | :----------------------------------------------------------------- | :------- |
| ipList   | [String] | Array of strings with the IP addresses or domain names to look up. | Yes      |
| fields   | String?  | Comma separated list of fields to return to the caller.            | No       |
| hostname | Bool     | Look up host name from IP address.                                 | No       |
| security | Bool     | Return extended security information about the IP address.         | No       |

#### Returns

This call returns an array of [IPLocation](/swift/models.html#iplocation) objects, and throws an [SalesflyError](/swift/errors.html#salesflyerror) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    let locations = try GeoLocation.getBulk(ipList: ["8.8.8.8", "google.com"])
    for loc in locations! {
        print("Country code:", loc.countryCode!)
    }
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print("Error:", error)
}
```

## Get location of caller

### public static func getCurrent(fields: String?, hostname: Bool, security: Bool) -> [IPLocation](/swift/models.html#iplocation)?

Gets geolocation of caller's current IP address.

#### Arguments

| Argument | Type    | Description                                                | Required |
| :------- | :------ | :--------------------------------------------------------- | :------- |
| fields   | String? | Comma separated list of fields to return to the caller.    | No       |
| hostname | Bool    | Look up host name from IP address.                         | No       |
| security | Bool    | Return extended security information about the IP address. | No       |

#### Arguments

This call returns an [IPLocation](/swift/models.html#iplocation) object, and throws an [SalesflyError](/swift/errors.html#salesflyerror) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    let location = try GeoLocation.getCurrent()
    print("Country code:", location!.countryCode!)
} catch let err as SalesflyError {
    print("Request failed:", err)
} catch {
    print("Error:", error)
}
```
