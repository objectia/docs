# Models

## APIUsage

```swift
public struct APIUsage  {
    public var geoLocationRequests: Int
    public var mailRequests: Int
}
```

## IPCurrency

```swift
public struct IPCurrency{
    public var code: String?
    public var numericCode: String?
    public var name: String?
    public var pluralName: String?
    public var symbol: String?
    public var nativeSymbol: String?
    public var decimalDigits: Int?
}
```

## IPLanguage

```swift
public struct IPLanguage {
    public var code: String?
    public var code2: String?
    public var name: String?
    public var nativeName: String?
    public var rtl: Bool?
}
```

## IPLocation

```swift
public struct IPLocation {
    public var ipAddress: String?
    public var type: String?   // ipv4 or ipv6
    public var hostname: String?
	public var continent: String?
	public var continentCode: String?
    public var country: String?
    public var countryNative: String?
    public var countryCode: String?
    public var countryCode3: String?
	public var capital: String?
	public var region: String?
	public var regionCode: String?
	public var city: String?
	public var postcode: String?
	public var latitude: Double? = 0.0
	public var longitude: Double? = 0.0
	public var phonePrefix: String?
    public var flag: String?
	public var flagEmoji: String?
    public var isEU: Bool? = false
	public var tld: String?
    public var currencies: [IPCurrency]?
    public var languages: [IPLanguage]?
    public var timezone: IPTimezone?
    public var security: IPSecurity?
}
```

## IPSecurity

```swift
public struct IPSecurity {
    public var isProxy: Bool?
    public var proxyType: String?
    public var isCrawler: Bool?
    public var crawlerName: String?
    public var crawlerType: String?
    public var isTOR: Bool?
    public var threatLevel: String?
    public var threatTypes: [String]?
}
```

## IPTimezone

```swift
public struct IPTimezone {
    public var id: String?
    public var localTime: String?
    public var gmtOffset: Int?
    public var code: String?
    public var daylightSaving: Bool?
}
```

## MailMessage

```swift
public struct MailMessage {
    public var from: String
    public var to: [String]
    public var subject: String
    public var text: String
    public var date: Date?
    public var html: String?
    public var attachments: [String]
    public var fromName: String?
    public var replyTo: String?
    public var cc: [String]
    public var bcc: [String]
    public var charset: String?
    public var encoding: String?
    public var tags: [String]
    public var requireTLS: Bool?
    public var verifyCertificate: Bool?
    public var openTracking: Bool?
    public var clickTracking: Bool?
    public var plainTextClickTracking: Bool?
    public var unsubscribeTracking: Bool?
    public var testMode: Bool?

    public init(from: String, to: [String], subject: String, text: String)
}
```

## MailReceipt

```swift
public struct MailReceipt {
    public var id: String
    public var acceptedRecipients: Int
    public var rejectedRecipients: Int
}
```

## PDFOptions

```swift
public struct PDFOptions {
    /// A URL pointing to a web page.
    public var documentURL : String?

    /// A string containing HTML.
    public var documentHTML : String?

    /// Name of the returned document. Defaults to 'document.
    public var documentName : String?

    /// Top margin, accepts values labeled with units. Example: '20mm'.
    public var marginTop : String?

    /// Bottom margin, accepts values labeled with units.
    public var marginBottom : String?

    /// Left margin, accepts values labeled with units.
    public var marginLeft : String?

    /// Right margin, accepts values labeled with units.
    public var marginRight : String?

    /// Paper orientation: 'landscape' or 'portrait'. Defaults to 'portrait'.
    public var orientation : String?

    /// Paper format. Defaults to 'A4'.
    public var pageFormat : String?

    /// Paper width, accepts values labeled with units. If set together with PageHeight, takes priority over PageFormat.
    public var pageWidth : String?

    /// Paper height, accepts values labeled with units. If set together with PageWidth, takes priority over PageFormat.
    public var pageHeight : String?

    /// Paper ranges to print, e.g., '1-5, 8, 11-13'. Defaults to the empty string, which means print all pages.
    public var pageRanges : String?

    /// Scale of the webpage rendering. Defaults to 1. Scale amount must be between 0.1 and 2.
    public var scale : Double?

    /// Header text to be displayed at the top of each page.
    public var headerText : String?

    /// Header alignment: 'left', 'center' or 'right'. Defaults to 'center.
    public var headerAlign : String?

    /// Left and right margin for header (only applied when using HeaderText).
    public var headerMargin : Int?

    /// HTML template for the header.
    public var headerHTML : String?

    /// A URL pointing to a HTML template for the header.
    public var headerURL : String?

    /// Footer text to be displayed at the bottom of each page.
    public var footerText : String?

    /// Footer alignment: 'left', 'center' or 'right'. Defaults to 'center.
    public var footerAlign : String?

    /// Left and right margin for footer (only applied using FooterText).
    public var footerMargin : Int?

    /// HTML template for the footer.
    public var footerHTML : String?

    /// A URL pointing to a HTML template for the footer.
    public var footerURL : String?

    /// Print background graphics. Defaults to false.
    public var printBackground : Bool?

    /// Give any CSS @page size declared in the page priority over what is declared in `width` and `height` or `format` options. Defaults to false, which will scale the content to fit the paper size.
    public var preferCSSPageSize : Bool?

    /// A URL pointing to a PNG or JPEG image that will be used as a watermark.
    public var watermarkURL : String?

    /// A string telling where to place the watermark on the page: 'topleft', 'topright', 'center', 'bottomleft', 'bottomright'. Defaults to 'center'.
    public var watermarkPosition : String?

    /// The number of pixels to shift the watermark image horizontally. Offset is given in pixels. Defaults to 0.
    public var watermarkOffsetX : Int?

    /// The number of pixels to shift the watermark image vertically. Offset is given in pixels. Defaults to 0.
    public var watermarkOffsetY : Int?

    /// The title of this document.
    public var title : String?

    /// The author of this document.
    public var author : String?

    /// The creator of this document.
    public var creator : String?

    /// The subject of this document.
    public var subject : String?

    /// An array of keywords associated with this document.
    public var keywords : [String]

    /// A RFC 3066 language-tag denoting the language of this document, or an empty string if the language is unknown.
    public var language : String?

    /// Encrypt the PDF document using one of following algorithms: 'aes-256', 'aes-128', 'rc4-128' or 'rc4-40'.
    public var encryption : String?

    /// Set the owner password (required when encryption is enabled).
    public var ownerPassword : String?

    /// Set the user password.
    public var userPassword : String?

    /// Set user permissions 'all' or 'none'. Defaults to 'all'.
    public var permissions : String?
}
```
