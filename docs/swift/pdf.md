# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

## Create PDF

### public static async Task&lt;byte[]&gt; CreateAsync(params [PDFOptions](/csharp/models.html#pdfoptions))

### public static func create(options: [PDFOptions](/swift/models.html#pdfoptions)) -> Data?

This endpoint converts HTML to PDF.

#### Arguments

| Attribute | Type                                        | Description                   | Required |
| :-------- | :------------------------------------------ | :---------------------------- | :------- |
| options   | [PDFOptions](/swift/models.html#pdfoptions) | PDF document creation options | Yes      |

#### Returns

This call returns an array of bytes containing the PDF document, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```swift
import Foundation
import Salesfly

extension String: Error {}

do {
    guard let apiKey = ProcessInfo.processInfo.environment["SALESFLY_APIKEY"] else {
        throw "API key not set"
    }
    try SalesflyClient.initialize(apiKey: apiKey)
    var options = PDFOptions()
    options.documentURL = "https://example.com/invoice.html"
    let buffer = try PDF.create(options: options)
    try buffer!.write(to: URL(string: "file:///Users/Me/document.pdf")!, options: .atomic)
} catch let err as SalesflyError {
    print("Failed to create PDF:", err)
} catch {
    print("Error:", error)
}
```
