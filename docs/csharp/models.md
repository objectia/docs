# Models

## APIUsage

```csharp
namespace Salesfly.Api
{
    /// APIUsage model contains the number of allowed and used requests.
    public class APIUsage
    {
        // The number of allowed requests you can make every month.
        public int Allowed { get; set; }

        // The number of requests you have made this month.
        public int Used { get; set; }
    }
}
```

## IPCurrency

```csharp
namespace Salesfly.Api
{
    public class IPCurrency
    {
        public string Code { get; set; }
        public string NumericCode { get; set; }
        public string Name { get; set; }
        public string PluralName { get; set; }
        public string Symbol { get; set; }
        public string NativeSymbol { get; set; }
        public int DecimalDigits { get; set; }
    }
}
```

## IPLanguage

```csharp
namespace Salesfly.Api
{
    public class IPLanguage
    {
        public string Code { get; set; }
        public string Code2 { get; set; }
        public string Name { get; set; }
        public string NativeName { get; set; }
        public bool RTL = false;
    }
}
```

## IPLocation

```csharp
namespace Salesfly.Api
{
    public class IPLocation
    {
        public string IpAddress { get; set; }
        public string Type { get; set; }
        public string Hostname { get; set; }
        public string Continent { get; set; }
        public string ContinentCode { get; set; }
        public string Country { get; set; }
        public string CountryNative { get; set; }
        public string CountryCode { get; set; }
        public string CountryCode3 { get; set; }
        public string Capital { get; set; }
        public string Region { get; set; }
        public string RegionCode { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public float Latitude = 0;
        public float Longitude = 0;
        public string PhonePrefix { get; set; }
        public IPCurrency[] Currencies { get; set; }
        public IPLanguage[] Languages { get; set; }
        public string Flag { get; set; }
        public string FlagEmoji { get; set; }
        public bool IsEU { get; set; }
        public string TLD { get; set; }
        public IPTimezone Timezone { get; set; }
        public IPSecurity Security { get; set; }
    }
}
```

## IPSecurity

```csharp
namespace Salesfly.Api
{
    public class IPSecurity
    {
        public bool IsProxy { get; set; }
        public string ProxyType { get; set; }
        public bool IsCrawler { get; set; }
        public string CrawlerName { get; set; }
        public string CrawlerType { get; set; }
        public bool IsTOR { get; set; }
        public string ThreatLevel { get; set; }
        public string[] ThreatTypes { get; set; }
    }
}
```

## IPTimezone

```csharp
namespace Salesfly.Api
{
    public class IPTimezone
    {
        public string id { get; set; }
        public DateTime LocalTime { get; set; }
        public int GMTOffset { get; set; }
        public string Code { get; set; }
        public bool DaylightSaving { get; set; }
    }
}
```

## MailMessage

```csharp
namespace Salesfly.Api
{
    public class MailMessage {
        // Constructor
        public MailMessage(string from, string subject, string text, params string[] to);
        // Properties
        public DateTime? Date { get; set; }
        public string From { get; set; }
        public string FromName { get; set; }
        public string ReplyTo { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public string HTML { get; set; }
        public List<string> Attachments { get; set; }
        public List<string> Tags { get; set; }
        public string Charset { get; set; }
        public string Encoding { get; set; }
        public bool? RequireTLS { get; set; }
        public bool? VerifyCertificate { get; set; }
        public bool? OpenTracking { get; set; }
        public bool? ClickTracking { get; set; }
        public bool? PlainTextClickTracking { get; set; }
        public bool? UnsubscribeTracking { get; set; }
        public bool? TestMode { get; set; }
        // Methods
        public void AddCc(params string[] cc);
        public void AddBcc(params string[] bcc);
        public void AddAttachment(string fileName);
        public void AddTag(string tag);
    }
}
```

## MailReceipt

```csharp
namespace Salesfly.Api
{
    public class MailReceipt
    {
        public string ID { get; }
        public int AcceptedRecipients { get; }
        public int RejectedRecipients { get; }
    }
}
```

## PDFOptions

```csharp
namespace Salesfly.Api
{
    /// Options for creating PDF documents.
        public class PDFOptions
    {
        // A URL pointing to a web page.
        public string DocumentURL { get; set; }

        // A string containing HTML.
        public string DocumentHTML { get; set; }

        // Name of the returned document. Defaults to 'document.
        public string DocumentName { get; set; }

        // Top margin, accepts values labeled with units. Example: '20mm'.
        public string MarginTop { get; set; }

        // Bottom margin, accepts values labeled with units.
        public string MarginBottom { get; set; }

        // Left margin, accepts values labeled with units.
        public string MarginLeft { get; set; }

        // Right margin, accepts values labeled with units.
        public string MarginRight { get; set; }

        // Paper orientation: 'landscape' or 'portrait'. Defaults to 'portrait'.
        public string Orientation { get; set; }

        // Paper format. Defaults to 'A4'.
        public string PageFormat { get; set; }

        // Paper width, accepts values labeled with units. If set together with PageHeight, takes priority over PagFormat.
        public string PageWidth { get; set; }

        // Paper height, accepts values labeled with units. If set together with PageWidth, takes priority over PageFormat.
        public string PageHeight { get; set; }

        // Paper ranges to print, e.g., '1-5, 8, 11-13'. Defaults to the empty string, which means print all pages.
        public string PageRanges { get; set; }

        // Scale of the webpage rendering. Defaults to 1. Scale amount must be between 0.1 and 2.
        public double Scale { get; set; }

        // Header text to be displayed at the top of each page.
        public string HeaderText { get; set; }

        // Header alignment: 'left', 'center' or 'right'. Defaults to 'center.
        public string HeaderAlign { get; set; }

        // Left and right margin for header (only applied when using HeaderText).
        public int HeaderMargin { get; set; }

        // HTML template for the header.
        public string HeaderHTML { get; set; }

        // A URL pointing to a HTML template for the header.
        public string HeaderURL { get; set; }

        // Footer text to be displayed at the bottom of each page.
        public string FooterText { get; set; }

        // Footer alignment: 'left', 'center' or 'right'. Defaults to 'center.
        public string FooterAlign { get; set; }

        // Left and right margin for footer (only applied using FooterText).
        public int FooterMargin { get; set; }

        // HTML template for the footer.
        public string FooterHTML { get; set; }

        // A URL pointing to a HTML template for the footer.
        public string FooterURL { get; set; }

        // Print background graphics. Defaults to false.
        public bool PrintBackground { get; set; }

        // Give any CSS @page size declared in the page priority over what is declared in `width` and `height` or `format` options. Defaults to false, which will scale the content to fit the paper size.
        public bool PreferCSSPageSize { get; set; }

        // A URL pointing to a PNG or JPEG image that will be used as a watermark.
        public string WatermarkURL { get; set; }

        // A string telling where to place the watermark on the page: 'topleft', 'topright', 'center', 'bottomleft', 'bottomright'. Defaults to 'center'.
        public string WatermarkPosition { get; set; }

        // The number of pixels to shift the watermark image horizontally. Offset is given in pixels. Defaults to 0.
        public int WatermarkOffsetX { get; set; }

        // The number of pixels to shift the watermark image vertically. Offset is given in pixels. Defaults to 0.
        public int WatermarkOffsetY { get; set; }

        // The title of this document.
        public string Title { get; set; }

        // The author of this document.
        public string Author { get; set; }

        // The creator of this document.
        public string Creator { get; set; }

        // The subject of this document.
        public string Subject { get; set; }

        // An array of keywords associated with this document.
        public string[] Keywords { get; set; }

        // A RFC 3066 language-tag denoting the language of this document, or an empty string if the language is unknown.
        public string Language { get; set; }

        // Encrypt the PDF document using one of following algorithms: 'aes-256', 'aes-128', 'rc4-128' or 'rc4-40'.
        public string Encryption { get; set; }

        // Set the owner password (required when encryption is enabled).
        public string OwnerPassword { get; set; }

        // Set the user password.
        public string UserPassword { get; set; }

        // Set user permissions 'all' or 'none'. Defaults to 'all'.
        public string Permissions { get; set; }
    }
}
```
