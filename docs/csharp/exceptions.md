# Exceptions

## APIException

```csharp
namespace Salesfly.Exceptions
{
    public class APIException : Exception
    {
        public APIException();
        public APIException(string message);
    }
}
```

## APIConnectionException

```csharp
namespace Salesfly.Exceptions
{
    public class APIConnectionException : APIException
    {
        public APIConnectionException(string message);
    }
}
```

## APITimeoutException

```csharp
namespace Salesfly.Exceptions
{
    public class APITimeoutException : APIException
    {
        public APITimeoutException(string message);
    }
}
```

## ResponseException

```csharp
namespace Salesfly.Exceptions
{
    public class ResponseException : APIException
    {
        public int Status { get; set; }
        public string Code { get; set; }
        public ResponseException(int status, string message, string code = null);
        public ResponseException(Error error);
    }
}
```
