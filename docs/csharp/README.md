# Introduction

<img src="/img/csharp.svg" width="50"/>

This is the documentation for the C# client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-csharp).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The C# client for Salesfly API requires .NET Core 3.0 or later to be installed. You can check if you already have .NET Core installed on your machine by opening up a command prompt or terminal and running the following command:

```bash
dotnet --version
```

You should see something like 3.0.100. If you receive an error message, you can [download .NET Core from Microsoft](https://www.microsoft.com/net/) and install it.

## Installation

The C# client for Salesfly API is published on [Nuget](https://www.nuget.org/packages/Salesfly), so you can install it with .NET CLI

```bash
dotnet add package Salesfly --version 1.0.0
```

or with Package Manager

```bash
PM> Install-Package Salesfly -Version 1.0.0
```

## Using

Now that we have .NET Core and salesfly-csharp installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

Create a new folder and create a new console application by typing in:

```bash
dotnet new console
dotnet add package Salesfly
```

Edit the file `Program.cs` so it has the following contents:

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("YOUR-API-KEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var location = await Api.GeoLocation.GetAsync("8.8.8.8");
            Console.WriteLine("Country code: " + location.CountryCode);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get location");
        }
    }
}
```

Save the file and enter the following command in terminal:

```bash
dotnet run
```

This will build the application and launch it. It should print the following on your screen:

```
Country code: US
```

## Error handling

The API client will throw an [APIException](/csharp/exceptions.html#apiexception) upon error, and you handle the errors in the normal way with a try-catch statement.
APIException has three subclasses: [APIConnectionException](/csharp/exceptions.html#apiconnectionexception), [APITimeoutException](/csharp/exceptions.html#apitimeoutexception) and [ResponseException](/csharp/exceptions.html#responseexception).

If you enter an invalid IP address, you will get an exception:

```csharp
try
{
    SalesflyClient.Init(apiKey);
    var location = await Api.GeoLocation.GetAsync("x");
} catch (APIConnectionException e) {
    Console.WriteLine("Failed to connect to API server");
} catch (APITimeoutException e) {
    Console.WriteLine("Connection to API server timed out");
} catch (ResponseException e) {
    Console.WriteLine("API request failed:");
    Console.WriteLine("* Status: {0}", e.Status);
    Console.WriteLine("* Code: {0}", e.Code);
    Console.WriteLine("* Message: {0}", e.Message);
}
```

You should use the `Code` attribute to determine the reason for the error. The example above will print the following:

```
API request failed:
* Status: 400
* Code: err-invalid-ip
* Message: Invalid IP address
```

You can find all the error codes in the [REST API documentation](/rest/#errors).
