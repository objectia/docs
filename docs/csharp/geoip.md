# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

It is located in the `Salesfly.Api` namespace in the class `Geolocation`.

## Get location by IP

### public static async Task&lt;[IPLocation](/csharp/models.html#iplocation)&gt; GetAsync(string ip, string fields = null, bool hostname = false, bool security = false)

Looks up geolocation for an IP address.

#### Arguments

| Argument | Type   | Description                                                | Required |
| :------- | :----- | :--------------------------------------------------------- | :------- |
| ip       | string | The IP address or domain name to look up.                  | Yes      |
| fields   | string | Comma separated list of fields to return to the caller.    | No       |
| hostname | bool   | Look up host name from IP address.                         | No       |
| security | bool   | Return extended security information about the IP address. | No       |

#### Returns

This call returns an [IPLocation](/csharp/models.html#iplocation) object, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var location = await Api.GeoLocation.GetAsync("8.8.8.8");
            Console.WriteLine("Country code: " + location.CountryCode);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get location");
        }
    }
}
```

## Get location of multiple IP addresses

### public static async Task&lt;List&lt;[IPLocation](/csharp/models.html#iplocation)&gt;&gt; GetBulkAsync(string[] ipList, string fields = null, bool hostname = false, bool security = false)

Looks up geolocation for multiple IP addresses.

#### Arguments

| Argument | Type     | Description                                                        | Required |
| :------- | :------- | :----------------------------------------------------------------- | :------- |
| ipList   | string[] | Array of strings with the IP addresses or domain names to look up. | Yes      |
| fields   | string   | Comma separated list of fields to return to the caller.            | No       |
| hostname | bool     | Look up host name from IP address.                                 | No       |
| security | bool     | Return extended security information about the IP address.         | No       |

#### Returns

This call returns an array of [IPLocation](/csharp/models.html#iplocation) objects, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var location = await Api.GeoLocation.GetBulkAsync(new String[]{"8.8.8.8", "google.com"});
            foreach (var l in locations) {
                Console.WriteLine("Country code: " + l.CountryCode);
            }
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get location");
        }
    }
}
```

## Get location of caller

### public static async Task&lt;[IPLocation](/csharp/models.html#iplocation)&gt; GetCurrentAsync(string fields = null, bool hostname = false, bool security = false)

Gets geolocation of caller's current IP address.

#### Arguments

| Argument | Type   | Description                                                | Required |
| :------- | :----- | :--------------------------------------------------------- | :------- |
| fields   | string | Comma separated list of fields to return to the caller.    | No       |
| hostname | bool   | Look up host name from IP address.                         | No       |
| security | bool   | Return extended security information about the IP address. | No       |

#### Arguments

This call returns an [IPLocation](/csharp/models.html#iplocation) object, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var location = await Api.GeoLocation.GetCurrentAsync();
            Console.WriteLine("Country code: " + location.CountryCode);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get location");
        }
    }
}
```
