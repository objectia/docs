# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### public static async Task&lt;[APIUsage](/csharp/models.html#apiusage)&gt; GetAsync()

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an [APIUsage](/csharp/models.html#apiusage) object, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var usage = await Api.Usage.GetAsync();
            Console.WriteLine("Allowed requests: " + usage.Allowed);
            Console.WriteLine("Used requests: " + usage.Used);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get usage");
        }
    }
}
```
