# Client

## Initialization

### public static void Init(string apiKey, int timeout=30)

Initializes the API client.

#### Arguments

| Parameter | Type    | Description                                           | Required |
| :-------- | :------ | :---------------------------------------------------- | :------- |
| apiKey    | string  | Your API key                                          | Yes      |
| timeout   | integer | Connection timeout in seconds (default is 30 seconds) | No       |

#### Returns

Nothing

#### Example

```csharp
var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
SalesflyClient.Init(apiKey);
```

## Version

### public static string Version

Gets API client version.

#### Arguments

None

#### Returns

The API client version string.

#### Example

```csharp
var version = SalesflyClient.Version;
Console.WriteLine("Client version: {0}", version);
```

This will print something like this on the screen:

```
Client version: 1.0.0
```

<!--
### Retry configuration

By default the client will retry each failed request four times. A failed request is usually a connection error, or a HTTP status code in the 500 series (except 501 Not Implemented).
The retry mechanism will perform an exponential wait based on the number of attempts and limited by the provided minimum and maximum durations. For example it will wait: 1s, 2s, 4s, 8s, and so on.

```js
client.RetryMax = 4						// Meaning one attempt + four retries
client.RetryWaitMin = 1 * time.Second
client.RetryWaitMax = 30 * time.Second
```
-->
