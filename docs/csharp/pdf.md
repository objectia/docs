# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

It is located in the `Salesfly.Api` namespace in the class `PDF`.

## Create PDF

### public static async Task&lt;byte[]&gt; CreateAsync([PDFOptions](/csharp/models.html#pdfoptions) options)

This endpoint converts HTML to PDF.

#### Arguments

| Attribute | Type                                         | Description                   | Required |
| :-------- | :------------------------------------------- | :---------------------------- | :------- |
| options   | [PDFOptions](/csharp/models.html#pdfoptions) | PDF document creation options | Yes      |

#### Returns

This call returns an array of bytes containing the PDF document, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using System.IO;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var options = new PDFOptions
            {
                DocumentURL = "https://example.com/invoice.html"
            };
            var buf = await Api.PDF.CreateAsync(options);
            File.WriteAllBytes("document.pdf", buf);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to get location");
        }
    }
}
```
