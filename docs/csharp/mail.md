# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail

### public static async Task&lt;[MailReceipt](/csharp/models.html#mailreceipt)&gt; SendAsync([MailMessage](/csharp/models.html#mailmessage) message)

Sends a mail message to one or more recipients.

#### Arguments

A [MailMessage](/csharp/models.html#mailmessage) object can contain the following attributes:

| Attribute                 | Type               | Description                                                                                                | Required |
| :------------------------ | :----------------- | :--------------------------------------------------------------------------------------------------------- | :------- |
| From                      | string             | The email address of the sender                                                                            | Yes      |
| To                        | List&lt;string&gt; | A list of recipient email addresses                                                                        | Yes      |
| Subject                   | string             | The message subject                                                                                        | Yes      |
| Text                      | string             | The plain text message text                                                                                | Yes      |
| HTML                      | string             | The HTML message text                                                                                      |
| FromName                  | string             | The name of sender                                                                                         |
| Date                      | DateTime?          | The time when message was sent                                                                             |
| ReplyTo                   | string             | The email address for replies                                                                              |
| Cc                        | List&lt;string&gt; | A list of cc email addresses                                                                               |
| Bcc                       | List&lt;string&gt; | A list of bcc email addresses                                                                              |
| Attachments               | List&lt;string&gt; | A list of file attachments (full path)                                                                     |
| Tags                      | List&lt;string&gt; | A list of tags                                                                                             |
| Charset                   | string             | The charset (default: "UTF-8")                                                                             |
| Encoding                  | string             | The encoding (default: "quoted-printable"). Other values: "base64", "8bit"                                 |
| RequireTLS \*             | bool?              | Whether to require TLS when connecting to the destination host                                             |
| VerifyCertificate \*      | bool?              | Whether to verify the SSL certificate of the remote host when connecting                                   |
| OpenTracking \*           | bool?              | A flag that enables or disables open tracking                                                              |
| ClickTracking             | bool?              | A flag that enables or disables click tracking (html message only)                                         |
| PlainTextClickTracking \* | bool?              | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| UnsubscribeTracking \*    | bool?              | A flag that specifies if an unsubscribe link should be added                                               |
| TestMode \*               | bool?              | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a [MailReceipt](/csharp/models.html#mailreceipt) object if the message was successfully processed, and throws an [APIException](/csharp/exceptions.html#apiexception) otherwise.

#### Example

```csharp
using System;
using Salesfly;
using Salesfly.Api;
using Salesfly.Exceptions;

class Program
{
    static async Task Main(string[] args)
    {
        var apiKey = Environment.GetEnvironmentVariable("SALESFLY_APIKEY");
        try
        {
            SalesflyClient.Init(apiKey);
            var message = new MailMessage("me@example.com", "Test", "This is a test", "you@example.com");
            message.AddAttachment("/Users/me/picture.png");
            var receipt = await Api.Mail.SendAsync(message);
            Console.WriteLine("Message ID: " + receipt.ID);
        } catch (ResponseException e) {
            Console.WriteLine("Failed to send mail");
        }
    }
}
```
