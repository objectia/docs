# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### public function get($ip, $options)

Looks up geolocation for an IP address.

#### Arguments

This call takes one or two arguments. The options argument is optional.

| Parameter        | Type   | Description                                                                               | Required |
| :--------------- | :----- | :---------------------------------------------------------------------------------------- | :------- |
| ip               | string | The IP address or domain name to look up.                                                 | Yes      |
| options.fields   | string | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | string | set to "true" if you want to reverse lookup ip address to hostname                        | No       |
| options.security | string | set to "true" if you want additional security info                                        | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) object, and returns an [APIException](/php/exceptions.html#apiexception) otherwise.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$options = array(
    "fields" => "country_code,hostname", // no spaces!
    "hostname" => "true", // must use string not bool!
);
$location = $client->geoip->get("8.8.8.8", $options);

echo "Country code: " . $location["country_code"];
echo "Hostname: " . $location["hostname"];
```

## Get location of multiple IP addresses

### public function getBulk($ip_list, $options)

Looks up geolocation for multiple IP addresses.

#### Arguments

This call takes one or two arguments. The options argument is optional.

| Parameter        | Type         | Description                                                                                  | Required |
| :--------------- | :----------- | :------------------------------------------------------------------------------------------- | :------- |
| ip_list          | string array | An array of strings with the IP addresses or domain names to look up.                        | Yes      |
| options.fields   | string       | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty. | No       |
| options.hostname | string       | Set to "true" if you want to reverse lookup ip address to hostname.                          | No       |
| options.security | string       | Set to "true" if you want additional security info.                                          | No       |

#### Returns

This call returns an array of [IP location](/rest/geoip.html#get-location-by-ip) object, and returns an [APIException](/php/exceptions.html#apiexception) otherwise.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);

$location = $client->geoip->getBulk(["8.8.8.8", "google.com"]);
foreach ($location as $loc) {
    echo "Country code: " . $loc["country_code"];
}
```

## Get location of caller

### public function getCurrent(\$options)

Gets the geolocation of caller's current IP address.

#### Arguments

This call takes an optional arguments.

| Parameter        | Type    | Description                                                                               | Required |
| :--------------- | :------ | :---------------------------------------------------------------------------------------- | :------- |
| options.fields   | string  | comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| options.hostname | boolean | set to true if you want to reverse lookup ip address to hostname                          | No       |
| options.security | boolean | set to true if you want additional security info                                          | No       |

#### Returns

This call returns an [IP location](/rest/geoip.html#get-location-by-ip) object, and returns an [APIException](/php/exceptions.html#apiexception) otherwise.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$location = $client->geoip->getCurrent();

echo "Country code: " . $location["country_code"];
```
