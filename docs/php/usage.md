# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage

### public function get() : mixed

This call retrieves the API usage for current month.

#### Arguments

None

#### Returns

This call returns an API usage array, and returns an [APIException](/php/exceptions.html#apiexception) otherwise.

The usage array has the following fields:

| Attribute | Type   | Description                                          |
| :-------- | :----- | :--------------------------------------------------- |
| allowed   | number | The number of allowed requests for the current month |
| used      | number | The number of used requests in the current month     |

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$usage = $client->usage->get();

echo "Number of allowed requests: " . $usage["allowed"];
echo "Number of used requests: " . $usage["used"];
```
