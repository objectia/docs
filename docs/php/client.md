# Client

## Initialize

### public function \_\_construct($apiKey, $timeout)

Creates a new API client.

#### Arguments

| Parameter | Type    | Description                                           | Required |
| :-------- | :------ | :---------------------------------------------------- | :------- |
| apiKey    | string  | Your API key                                          | Yes      |
| timeout   | integer | Connection timeout in seconds (default is 30 seconds) | No       |

#### Returns

This call returns the API client object.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]
$client = new Client($apiKey);
```

## Get version

### public function getVersion() : string

Gets the API client version

#### Arguments

None

#### Returns

This call returns the version number as a string.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]
$client = new Client($apiKey);
$version = $client->getVersion();
echo "Version: " . $version;
```
