# PHP client for Salesfly API

<img src="/php.png" width="50"/>

## Introduction

This is the documentation for the PHP client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-php).
We have API clients in several other programming languages hosted on GitHub.

### Requirements

The PHP client for Salesfly API requires PHP version 7.1 or later. If you’re using a Mac or Linux machine, you probably already have PHP installed. You can check this by opening up a terminal and running the following command:

```bash
php --version
```

If it is not installed, follow the [PHP installation instructions](http://php.net/manual/en/install.php).

If you're using a Windows machine, please follow the [official PHP tutorial to install PHP](http://php.net/manual/en/install.windows.php).

::: danger IMPORTANT
While many versions of PHP will work for this quickstart, please pay attention to [supported PHP releases](http://php.net/supported-versions.php). Always update unsupported versions when doing web development, as older versions will not receive security updates.
:::

### Installation

The easiest way to install salesfly-php is using Composer. Composer is the de facto standard package manager for PHP web development.
If you haven't yet installed it, here are the installation instructions for your platform:

- [Install Composer for Mac OSX, Linux, or Unix](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
- [Install Composer for Windows](https://getcomposer.org/doc/00-intro.md#installation-windows)

```bash
composer require salesfly/client
```

Alternatively, you can create a file named composer.json. In that file, add:

```json
{
  "require": {
    "salesfly/client": "^1.0"
  }
}
```

Then run

```bash
composer install
```

And composer will install the latest 1.x version of the PHP client for Salesfly API.

### Using

Now that we have PHP and salesfly-php installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```php
<?php
require __DIR__ . "/vendor/autoload.php";
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$location = $client->geoip->get("8.8.8.8");

echo "Country code: " . $location["country_code"];
```

### Error handling

Handle errors with a rescue block:

```ruby

# If you enter an invalid IP address, you get an error
begin
    location = client.geoip.get("288.8.8.8")
rescue Salesfly::ResponseError => e
    if e.code == "err-invalid-ip"
		# Handle error...
	else
		# Other error...
end
```

You will find all the error codes in the [REST API documentation](/rest/#errors).

## Client

### Client(api_key=None, timeout=30)

Create a new API client.

The client provides a Logger facility, so you can attach your own logger. Here we are using the Console API as logger. You can write your own custom
logger, but you have to follow the same interface as the Console logger.

| Parameter | Type    | Description                              |
| :-------- | :------ | :--------------------------------------- |
| api_key   | string  | Your API key (required)                  |
| timeout   | integer | Connection timeout in seconds (optional) |

#### Example

```python
import os
import salesfly

APIKEY = os.environ.get("SALESFLY_APIKEY")

client = salesfly.Client(api_key=API_KEY, timeout=60)
```

### client.version()

Get API client version

#### Example

```python
v = client.version()
print("Client version: {0}".format(v))   # prints "1.0.0"
```

<!--
### Retry configuration

By default the client will retry each failed request four times. A failed request is usually a connection error, or a HTTP status code in the 500 series (except 501 Not Implemented).
The retry mechanism will perform an exponential wait based on the number of attempts and limited by the provided minimum and maximum durations. For example it will wait: 1s, 2s, 4s, 8s, and so on.

```js
client.RetryMax = 4						// Meaning one attempt + four retries
client.RetryWaitMin = 1 * time.Second
client.RetryWaitMax = 30 * time.Second
```
-->

## Geolocation API

### client.geoip.get(ip, fields=None, hostname=false, security=false)

Look up geolocation for an IP address.

| Parameter | Type   | Description                                                           |
| :-------- | :----- | :-------------------------------------------------------------------- |
| ip        | string | The IP address or domain name to look up.                             |
| fields    | string | Comma separated list of fields to return to the caller (optional).    |
| hostname  | bool   | Look up host name from IP address (optional).                         |
| security  | bool   | Return extended security information about the IP address (optional). |

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    location = client.geoip.get("8.8.8.8")
    print("Country code: {0}".format(location["country_code"]))
except:
    print("Failed to get geo location")
```

### client.geoip.get_bulk(ip_list, fields=None, hostname=false, security=false)

Look up geolocation for multiple IP addresses.

| Parameter | Type   | Description                                                           |
| :-------- | :----- | :-------------------------------------------------------------------- |
| ip_list   | list   | List of strings with the IP addresses or domain names to look up.     |
| fields    | string | Comma separated list of fields to return to the caller (optional).    |
| hostname  | bool   | Look up host name from IP address (optional).                         |
| security  | bool   | Return extended security information about the IP address (optional). |

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    locations = client.geoip.get_bulk(["8.8.8.8,google.com"])
    for i in range(len(locations)):
        print("Country code: {0}".format(locations[i]["country_code"]))
except:
    print("Failed to get geo location")
```

### client.geoip.get_current(fields=None, hostname=false, security=false)

Get geolocation of caller's current IP address.

| Parameter | Type   | Description                                                           |
| :-------- | :----- | :-------------------------------------------------------------------- |
| fields    | string | Comma separated list of fields to return to the caller (optional).    |
| hostname  | bool   | Look up host name from IP address (optional).                         |
| security  | bool   | Return extended security information about the IP address (optional). |

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    location = client.geoip.get_current()
    print("Country code: {0}".format(location["country_code"]))
except:
    print("Failed to get geo location")
```

## Usage API

### client.usage.get()

Retrieve API usage for current month.

#### Example

```python
import salesfly

client = salesfly.Client(api_key="YOUR-API-KEY")

try:
    usage = client.usage.get()
    print("Number of GeoLocation requests: {0}".format(usage["geoip_requests"]))
except:
    print("Failed to get API usage")
```
