# Introduction

<img src="/img/php.png" width="50"/>

This is the documentation for the PHP client for Salesfly&reg; API. You can find the source code for the client on [GitHub](https://github.com/salesfly/salesfly-php).
We have API clients in several other programming languages hosted on GitHub.

## Requirements

The PHP client for Salesfly API requires PHP version 7.1 or later. If you’re using a Mac or Linux machine, you probably already have PHP installed. You can check this by opening up a terminal and running the following command:

```bash
php --version
```

If it is not installed, follow the [PHP installation instructions](http://php.net/manual/en/install.php).

If you're using a Windows machine, please follow the [official PHP tutorial to install PHP](http://php.net/manual/en/install.windows.php).

::: danger IMPORTANT
While many versions of PHP will work for this quickstart, please pay attention to [supported PHP releases](http://php.net/supported-versions.php). Always update unsupported versions when doing web development, as older versions will not receive security updates.
:::

## Installation

The easiest way to install salesfly-php is using Composer. Composer is the de facto standard package manager for PHP web development.
If you haven't yet installed it, here are the installation instructions for your platform:

- [Install Composer for Mac OSX, Linux, or Unix](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
- [Install Composer for Windows](https://getcomposer.org/doc/00-intro.md#installation-windows)

```bash
composer require salesfly/client
```

Alternatively, you can create a file named composer.json. In that file, add:

```json
{
  "require": {
    "salesfly/client": "^1.0"
  }
}
```

Then run

```bash
composer install
```

And composer will install the latest 1.x version of the PHP client for Salesfly API.

## Using

Now that we have PHP and salesfly-php installed, we can start using the API. In the example below we will look up the geolocation of the IP address 8.8.8.8.

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$location = $client->geoip->get("8.8.8.8");

echo "Country code: " . $location["country_code"];
```

## Error handling

The API client will throw an [APIException](/php/exceptions.html#apiexception) upon error, and you handle the errors in the normal way with a try-catch statement.
APIException has three subclasses: [APIConnectionException](/php/exceptions.html#apiconnectionexception), [APITimeoutException](/php/exceptions.html#apitimeoutexception) and [ResponseException](/php/exceptions.html#responseexception).

If you enter an invalid IP address, you will get an exception:

```php
<?php
use Salesfly\Client;
use Salesfly\Exceptions\ResponseException;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);

try {
    $location = $this->client->geolocation->get("x");
} catch (ResponseException $e) {
    echo "API request failed:";
    echo "* Status: " . $e->getStatus();
    echo "* Code: " . $e->getErrorCode();
    echo "* Message: " . $e->getMessage();
}
```

The example above will print the following:

```
API request failed:
* Status: 400
* Code: err-invalid-ip
* Message: Invalid IP address
```

You should use the `getErrorCode()` method to determine the reason for the error. Please observe that this is not the same method as
`Exception::getCode()`.

You will find all the error codes in the [REST API documentation](/rest/#errors).
