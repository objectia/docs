# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

## Create PDF

### public function create(\$options : mixed) : mixed

This endpoint converts HTML to PDF.

#### Arguments

The options array can contain the following attributes:

| Attribute            | Type         | Description                                                                                                                                                                                     | Required |
| :------------------- | :----------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------- |
| document_url         | string       | A URL pointing to a web page.                                                                                                                                                                   | Yes \*   |
| document_html        | string       | A string containing HTML code.                                                                                                                                                                  | Yes \*   |
| document_name        | string       | Name of the returned document. Defaults to 'document.pdf'                                                                                                                                       | No       |
| margin_top           | string       | Top margin, accepts values labeled with units. Example: '20mm'.                                                                                                                                 | No       |
| margin_bottom        | string       | Bottom margin, accepts values labeled with units.                                                                                                                                               | No       |
| margin_left          | string       | Left margin, accepts values labeled with units.                                                                                                                                                 | No       |
| margin_right         | string       | Right margin, accepts values labeled with units.                                                                                                                                                | No       |
| orientation          | string       | Paper orientation: 'landscape' or 'portrait'. Defaults to 'portrait'.                                                                                                                           | No       |
| page_format          | string       | Paper format. Defaults to 'A4'.                                                                                                                                                                 | No       |
| page_width           | string       | Paper width, accepts values labeled with units. If set together with page_height, takes priority over page_format.                                                                              | No       |
| page_height          | string       | Paper height, accepts values labeled with units. If set together with page_width, takes priority over page_format.                                                                              | No       |
| page_ranges          | string       | Paper ranges to print, e.g., '1-5, 8, 11-13'. Defaults to the empty string, which means print all pages.                                                                                        | No       |
| scale                | number       | Scale of the webpage rendering. Defaults to 1. Scale amount must be between 0.1 and 2.                                                                                                          | No       |
| header_text          | string       | Header text to be displayed at the top of each page.                                                                                                                                            | No       |
| header_align         | string       | Header alignment: 'left', 'center' or 'right'. Defaults to 'center.                                                                                                                             | No       |
| header_margin        | number       | Left and right margin for header (only applied when using header_text).                                                                                                                         | No       |
| header_html          | string       | HTML template for the header.                                                                                                                                                                   | No       |
| header_url           | string       | A URL pointing to a HTML template for the header.                                                                                                                                               | No       |
| footer_text          | string       | Footer text to be displayed at the bottom of each page.                                                                                                                                         | No       |
| footer_align         | string       | Footer alignment: 'left', 'center' or 'right'. Defaults to 'center.                                                                                                                             | No       |
| footer_margin        | number       | Left and right margin for footer (only applied using footer_text).                                                                                                                              | No       |
| footer_html          | string       | HTML template for the footer.                                                                                                                                                                   | No       |
| footer_url           | string       | A URL pointing to a HTML template for the footer.                                                                                                                                               | No       |
| print_background     | boolean      | Print background graphics. Defaults to false.                                                                                                                                                   | No       |
| prefer_css_page_size | boolean      | Give any CSS @page size declared in the page priority over what is declared in `width` and `height` or `format` options. Defaults to false, which will scale the content to fit the paper size. | No       |
| watermark_url        | string       | A URL pointing to a PNG or JPEG image that will be used as a watermark.                                                                                                                         | No       |
| watermark_position   | string       | A string telling where to place the watermark on the page: 'topleft', 'topright', 'center', 'bottomleft', 'bottomright'. Defaults to 'center'.                                                  | No       |
| watermark_offset_x   | number       | The number of pixels to shift the watermark image horizontally. Offset is given in pixels. Defaults to 0.                                                                                       | No       |
| watermark_offset_y   | number       | The number of pixels to shift the watermark image vertically. Offset is given in pixels. Defaults to 0.                                                                                         | No       |
| title                | string       | The title of this document.                                                                                                                                                                     | No       |
| author               | string       | The author of this document.                                                                                                                                                                    | No       |
| creator              | string       | The creator of this document.                                                                                                                                                                   | No       |
| subject              | string       | The subject of this document.                                                                                                                                                                   | No       |
| keywords             | string array | An array of keywords associated with this document.                                                                                                                                             | No       |
| language             | string       | An RFC 3066 language-tag denoting the language of this document, or an empty string if the language is unknown.                                                                                 | No       |
| encryption           | string       | Encrypt the PDF document using one of following algorithms: 'aes-256', 'aes-128', 'rc4-128' or 'rc4-40'.                                                                                        | No       |
| owner_password       | string       | Set the owner password (required when encryption is enabled).                                                                                                                                   | No       |
| user_password        | string       | Set the user password.                                                                                                                                                                          | No       |
| permissions          | string       | Set user permissions 'all' or 'none'. Defaults to 'all'.                                                                                                                                        | No       |

> \* = You must supply either a `document_url` or a `document_html`a string containing raw HTML markup.

#### Returns

This call returns a PDF document if the request was successfully processed. An [error](/rest/#errors) is returned if something goes wrong.

#### Example

```php
<?php
use Salesfly\Client;

$apiKey = $_ENV["SALESFLY_APIKEY"]

$client = new Client($apiKey);
$options = [
    "document_url" => "https://example.com/invoice.html",
];
$buffer = $client->pdf->create($options);
file_put_contents("document.pdf", $buffer);
```
