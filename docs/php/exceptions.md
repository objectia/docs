# Errors

## APIException

```php
<?php

namespace Salesfly\Exceptions;

class APIException extends \Exception
```

## APIConnectionException

```php
<?php

namespace Salesfly\Exceptions;

class APIConnectionException extends APIException
```

## APITimeoutException

```php
<?php

namespace Salesfly\Exceptions;

class APITimeoutException extends APIException
```

## ResponseException

```php
<?php

namespace Salesfly\Exceptions;

class ResponseException extends APIException
{
    public function __construct($status, $message, $code = null, \Exception $previous = null);
    public function getStatus();
    public function getErrorCode();
}
```
