# Introduction

Welcome to the Salesfly&reg; REST API.

## Endpoints

All API requests are made to <code>api.salesfly.com</code> and all requests are served over HTTPS.

```http
https://api.salesfly.com/v1
```

::: danger IMPORTANT
API calls made over plain HTTP are not supported and will fail.
:::

## Versioning

As we develop future versions of our API that are not backwards-compatible, we will leave the old version running and create a new url for the latest version. We will retain support for obsolete versions for a generous period of time and will send email notifications of any changes.

The current API version is <code>v1</code>. Future versions will be v2, v3, etc.

## Authentication

All requests to the API must be authenticated with an API key. You can submit the API key in the HTTP request header, or you can specify the access token as a HTTP URL query parameter.

::: tip
If you do not have an API Key, you can easily generate one by heading over to the [API settings page](https://salesfly.com/account).
:::

#### In the HTTP Authorization header:

```http
Authorization: Bearer YOUR-API-KEY
```

#### In the URL

Add <code>api_key=YOUR-API-KEY</code> to the URL:

```http
https://api.salesfly.com/v1/geoip/8.8.8.8?api_key=YOUR-API-KEY
```

::: danger IMPORTANT
API requests without authentication will fail.
:::

## Errors

Salesfly uses conventional HTTP status response codes to indicate the success or failure of an API request. In general: Status codes in the <code>2XX</code> range indicate success. Status codes in the <code>4XX</code>
range indicate an error that failed given the information provided (e.g., a required parameter was omitted, a charge failed, etc.). Status codes in the <code>5XX</code> range indicate an error with Salesfly's servers (these are rare).

The body of an error response:

```json
{
  "status": 404,
  "success": false,
  "code": "err-ip-not-found",
  "message": "IP address not found"
}
```

<code>status</code> is the HTTP status code, <code>success</code> is true upon success and false if there was an error. <code>message</code> is a descriptive error message string, and finally <code>code</code> is the error code (see below for a list of error codes).

### Error codes

| Code                                                                                | Description                                                                                       |
| :---------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------ |
| <span style="white-space: nowrap;"><code>err-access-denied</code></span>            | Access was denied to the API endpoint. Check your API key permissions or IP address restrictions. |
| <span style="white-space: nowrap;"><code>err-account-suspended</code></span>        | Your account has been suspended due to a payment problem.                                         |
| <span style="white-space: nowrap;"><code>err-bad-gateway</code></span>              | Bad gateway (DNS or routing problem, 502).                                                        |
| <span style="white-space: nowrap;"><code>err-date-before-2000</code></span>         | Date must be after January 1, 2000.                                                               |
| <span style="white-space: nowrap;"><code>err-date-must-be-in-the-past</code></span> | Date must be in the past.                                                                         |
| <span style="white-space: nowrap;"><code>err-https-required</code></span>           | You must use HTTPS to access the API.                                                             |
| <span style="white-space: nowrap;"><code>err-internal-server-error</code></span>    | Something went wrong on our side (500).                                                           |
| <span style="white-space: nowrap;"><code>err-invalid-amount</code></span>           | Amount must be a number larger than zero (ie. 12345.00).                                          |
| <span style="white-space: nowrap;"><code>err-invalid-apikey</code></span>           | The API key provides was not valid (wrong format or not found in our database).                   |
| <span style="white-space: nowrap;"><code>err-invalid-base-currency</code></span>    | Invalid base currency.                                                                            |
| <span style="white-space: nowrap;"><code>err-invalid-date</code></span>             | Invalid date (format must be YYYY-MM-DD).                                                         |
| <span style="white-space: nowrap;"><code>err-invalid-dest-currency</code></span>    | Invalid destination currency.                                                                     |
| <span style="white-space: nowrap;"><code>err-invalid-end-date</code></span>         | Invalid end date (format must be YYYY-MM-DD).                                                     |
| <span style="white-space: nowrap;"><code>err-invalid-ip</code></span>               | You provided an invalid IP address for Geolocation lookup.                                        |
| <span style="white-space: nowrap;"><code>err-invalid-param</code></span>            | The request has a missing or wrong input parameter.                                               |
| <span style="white-space: nowrap;"><code>err-invalid-request</code></span>          | The request sent to the API server was incomplete or had invalid parameters.                      |
| <span style="white-space: nowrap;"><code>err-invalid-sender</code></span>           | Not allowed to send from this address.                                                            |
| <span style="white-space: nowrap;"><code>err-invalid-source-currency</code></span>  | Invalid source currency.                                                                          |
| <span style="white-space: nowrap;"><code>err-invalid-start-date</code></span>       | Invalid start date (format must be YYYY-MM-DD) and must be after end date.                        |
| <span style="white-space: nowrap;"><code>err-invalid-timespan</code></span>         | Max timespan is one year (365 days).                                                              |
| <span style="white-space: nowrap;"><code>err-ip-not-found</code></span>             | The specified IP address was not found in the GeoIP database.                                     |
| <span style="white-space: nowrap;"><code>err-malware-detected</code></span>         | Malware has been detected in outgoing mail.                                                       |
| <span style="white-space: nowrap;"><code>err-missing-apikey</code></span>           | You forgot to provide an API key.                                                                 |
| <span style="white-space: nowrap;"><code>err-missing-dkim</code></span>             | Missing or invalid DKIM DNS record for sending domain.                                            |
| <span style="white-space: nowrap;"><code>err-missing-dmarc</code></span>            | Missing or invalid DMARC DNS record for sending domain.                                           |
| <span style="white-space: nowrap;"><code>err-missing-spf</code></span>              | Missing or invalid SPF DNS record for sending domain.                                             |
| <span style="white-space: nowrap;"><code>err-no-currency-data</code></span>         | No currency data for chosen date.                                                                 |
| <span style="white-space: nowrap;"><code>err-no-data</code></span>                  | No data available.                                                                                |
| <span style="white-space: nowrap;"><code>err-no-recipients</code></span>            | No valid mail recipients.                                                                         |
| <span style="white-space: nowrap;"><code>err-no-route</code></span>                 | Route not found.                                                                                  |
| <span style="white-space: nowrap;"><code>err-not-modified</code></span>             | Not modified (304)                                                                                |
| <span style="white-space: nowrap;"><code>err-pro-feature</code></span>              | This feature is not available for your plan (upgrade to the Pro plan to continue).                |
| <span style="white-space: nowrap;"><code>err-rate-limit</code></span>               | Too may requests - rate limit exceeded.                                                           |
| <span style="white-space: nowrap;"><code>err-same-currency</code></span>            | Source and destination currencies must be different.                                              |
| <span style="white-space: nowrap;"><code>err-service-unavailable</code></span>      | Service unavailable (503).                                                                        |
| <span style="white-space: nowrap;"><code>err-spam-detected</code></span>            | Outgoing mail has been flagged as spam.                                                           |
| <span style="white-space: nowrap;"><code>err-too-many-recipients</code></span>      | Too many mail recipients.                                                                         |
| <span style="white-space: nowrap;"><code>err-unexpected</code></span>               | Unexpected response from server.                                                                  |
| <span style="white-space: nowrap;"><code>err-usage-exceeded</code></span>           | You have exceeded your API usage limit.                                                           |

## Rate limits

API access rate limits are applied at a per-key basis in unit time. Access to the API using a key is limited to 60 requests per minute for BASIC accounts, and 600 requests per minute for PRO accounts.
In addition, every API response is accompanied by the following set of headers to identify the status of your consumption.

| Header                                                                       | Description                                                                                        |
| :--------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------- |
| <span style="white-space: nowrap;"><code>X-RateLimit-Limit</code></span>     | The maximum number of requests that the consumer is permitted to make per minute.                  |
| <span style="white-space: nowrap;"><code>X-RateLimit-Remaining</code></span> | The number of requests remaining in the current rate limit window.                                 |
| <span style="white-space: nowrap;"><code>X-RateLimit-Reset</code></span>     | The time at which the current rate limit window resets in UTC epoch seconds.                       |
| <span style="white-space: nowrap;"><code>X-RateLimit-Delay</code></span>     | The number of seconds until the rate counter is reset (only present when limit has been exceeded). |

Once you hit the rate limit, you will receive a response similar to the following JSON, with a status code of <code>429</code>.

```json
{
  "status": 429,
  "success": false,
  "code": "err-rate-limit",
  "message": "Too may requests - rate limit exceeded"
}
```

## Making requests

The API returns data in either JSON or XML format. JSON is the default format. You specify the format of the return data using the `Accept` header.
You can also specify the data format using HTTP URL query parameter `format`. This is particulary useful for GET requests where you cannot set HTTP headers.

#### The Accept header:

```http
Accept: application/json
```

or

```http
Accept: application/xml
```

#### In the URL

Add <code>format=json</code> or <code>format=xml</code> to the URL:

```http
https://api.salesfly.com/v1/geoip/8.8.8.8
  ?api_key=YOUR-API-KEY
  &format=json
```

or

```http
https://api.salesfly.com/v1/geoip/8.8.8.8
  ?api_key=YOUR-API-KEY
  &format=xml
```

## POSTing data

For POST/PATCH/PUT requests you can upload data using JSON, XML, form URL encoded or multipart form data. You specify the type of content using the `Content-Type` header.

#### JSON:

```http
Content-Type: application/json
```

```json
{
  "from": "me@example.com",
  "to": ["john@example.com", "steve@example.com"],
  "subject": "Test",
  "text": "This is a test"
}
```

#### XML:

```http
Content-Type: application/xml
```

You must enclose an XML request inside a `<request>` tag.

```xml
<request>
	<from>me@example.com</from>
	<to>john@example.com</to>
	<to>steve@example.com</to>
	<subject>Test</subject>
	<text>This is a test</text>
</request>
```

#### Form URL encoded:

```http
Content-Type: application/x-www-form-urlencoded
```

#### Multipart form data:

```http
Content-Type: multipart/form-data; boundary="someboundary"
```

## Responses

#### Single-item responses will be in this format:

JSON:

```json
{
  "status": 200,
  "success": true,
  "data": {
    "id": 1,
    "name": "test"
  }
}
```

or XML:

```xml
<result>
    <status>200</status>
    <success>true</success>
    <data>
        <id>1</id>
        <name>test</name>
    </data>
</result>
```

Most endpoints return a single model of data.

#### Multi-item responses will be in this format:

JSON:

```json
{
  "status": 200,
  "success": true,
  "data": [
    {
      "id": 1,
      "name": "test1"
    },
    {
      "id": 2,
      "name": "test2"
    }
  ]
}
```

or XML:

```xml
<result>
    <status>200</status>
    <success>true</success>
    <dataset>
       <data index="0">
          <id>1</id>
          <name>test1</name>
       </data>
       <data index="1">
          <id>2</id>
          <name>test2</name>
       </data>
    </dataset>
</result>
```

<b>NOTE</b>: The data index starts from zero for XML arrays.

## JSON Callbacks

The Salesfly API also supports JSONP callbacks. To use this feature, add <code>callback=CALLBACK_FUNCTION</code> to the HTTP request, and the result set will be returned as the callback function you specified.

Example request:

```http
https://api.salesfly.com/v1/geoip/8.8.8.8
  ?api_key=YOUR-API-KEY
  &callback=CALLBACK_FUNCTION
```

Example response:

```json
CALLBACK_FUNCTION({
  "data": {
    "country_code": "US"
  },
  "status": 200,
  "success": true
})
```

## ETags / Cache Control

The ETag or entity tag is part of the HTTP protocol. It is one of several mechanisms that HTTP provides for Web cache validation, which allows a client to make conditional requests. This allows caches to be more efficient and saves bandwidth, as a Web server does not need to send a full response if the content has not changed.

An ETag is an opaque identifier assigned by a web server to a specific version of a resource found at a URL. If the resource content at that URL ever changes, a new and different ETag is assigned. Used in this manner ETags are similar to fingerprints, and they can be quickly compared to determine if two versions of a resource are the same or not.

The [Currency API](/rest/currency.html) supports ETags, and this allows you to check whether or not currency rates have changed since your last API request. If the rates have not been modified, your API response will be considerably smaller in size than if they have. Practically, ETags provide a mechanism to cache exchange rate data as long as it is not updated.

This is how you use it in your client code:

#### 1. Save the latest API response

Each request to the Currency API will include an ETag and a Date in the HTTP response headers.

The Etag is a unique alphanumeric identifier for the data returned in the response, and the Date is the time at which the data was last modified. The date and time format is defined in
[RFC7231](https://tools.ietf.org/html/rfc7231#section-7.1.1.1).

Example:

```http
Date: Mon, 16 Mar 2020 10:15:46 GMT
ETag: "e33161ba2244ff74b9c9f1739cebec80"
```

#### 2. Add the "If-None-Match" header

The next time you make a request to the same API URL, add the `If-None-Match` header, with the value set to the ETag you saved from the previous request. Remember to wrap the value in double quotation '"' marks.

You also need to send an `If-Modified-Since` header, which will be the Date value from the last successful request.

If continue to use the example above, your two request headers would look like this:

```http
If-None-Match: "e33161ba2244ff74b9c9f1739cebec80"
If-Modified-Since: Mon, 16 Mar 2020 10:15:46 GMT
```

#### 3. If not modified, use cached data

If the data have not been updated since your last request, the response status code will be `304 – Not Modified`, and no data will be returned.

You can now safely use the cached API response from your previous successful request.

#### 4. If changed, cache the new response

If the rates have been modified since your last request, the latest data will returned as usual, along with new `ETag` and `Date` headers.

Go back to step 1.

::: warning NOTE
Although ETags help reduce bandwidth for your users, all API requests still count towards your monthly usage – even if the data has not changed since your last request.
:::

## Formatted output

By default the output from the Salesfly API is not formatted in any way. In order to improve readability you can specify that you want the output
to be formatted and indented.

To enable this feature, add <code>indented=true</code> to the API request:

```http
https://api.salesfly.com/v1/geoip/8.8.8.8
  ?api_key=YOUR-API-KEY
  &indented=true
```

::: warning NOTE
Indented output is larger than normal output and should only be used for debugging purposes.
:::

## Legal notices

Your use and access to the API is expressly conditioned on your compliance with the policies, restrictions, and other provisions related to the API set forth in our [Terms of Service](https://salesfly.com/terms) and the [Privacy Policy](https://salesfly.com/privacy), in all uses of the API. If we believes that you have or attempted to violate any term, condition, or the spirit of these policies or agreements, your right to access and use the API may be temporarily or permanently revoked.

Salesfly is a registered trademark of UAB Salesfly.
