# PDF API

The PDF API is an easy way to create PDF documents from HTML.

We support two sources, either a URL or a string of HTML code.

A PDF document is created from an HTML page and sent back to you. The PDF is not stored or cached in our system.

## Create PDF

### <span class="endpoint"><span class="post">POST</span> /v1/pdf/create</span>

This endpoint converts HTML to PDF.

#### Arguments

A request object can contain the following attributes:

| Attribute            | Type     | Description                                                                                                                                                                                     | Required |
| :------------------- | :------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------- |
| document_url         | string   | A URL pointing to a web page.                                                                                                                                                                   | Yes \*   |
| document_html        | string   | A string containing HTML code.                                                                                                                                                                  | Yes \*   |
| document_name        | string   | Name of the returned document. Defaults to 'document.pdf'                                                                                                                                       | No       |
| margin_top           | string   | Top margin, accepts values labeled with units. Example: '20mm'.                                                                                                                                 | No       |
| margin_bottom        | string   | Bottom margin, accepts values labeled with units.                                                                                                                                               | No       |
| margin_left          | string   | Left margin, accepts values labeled with units.                                                                                                                                                 | No       |
| margin_right         | string   | Right margin, accepts values labeled with units.                                                                                                                                                | No       |
| orientation          | string   | Paper orientation: 'landscape' or 'portrait'. Defaults to 'portrait'.                                                                                                                           | No       |
| page_format          | string   | Paper format. Defaults to 'A4'.                                                                                                                                                                 | No       |
| page_width           | string   | Paper width, accepts values labeled with units. If set together with page_height, takes priority over page_format.                                                                              | No       |
| page_height          | string   | Paper height, accepts values labeled with units. If set together with page_width, takes priority over page_format.                                                                              | No       |
| page_ranges          | string   | Paper ranges to print, e.g., '1-5, 8, 11-13'. Defaults to the empty string, which means print all pages.                                                                                        | No       |
| scale                | number   | Scale of the webpage rendering. Defaults to 1. Scale amount must be between 0.1 and 2.                                                                                                          | No       |
| header_text          | string   | Header text to be displayed at the top of each page.                                                                                                                                            | No       |
| header_align         | string   | Header alignment: 'left', 'center' or 'right'. Defaults to 'center.                                                                                                                             | No       |
| header_margin        | number   | Left and right margin for header (only applied when using header_text).                                                                                                                         | No       |
| header_html          | string   | HTML template for the header.                                                                                                                                                                   | No       |
| header_url           | string   | A URL pointing to a HTML template for the header.                                                                                                                                               | No       |
| footer_text          | string   | Footer text to be displayed at the bottom of each page.                                                                                                                                         | No       |
| footer_align         | string   | Footer alignment: 'left', 'center' or 'right'. Defaults to 'center.                                                                                                                             | No       |
| footer_margin        | number   | Left and right margin for footer (only applied using footer_text).                                                                                                                              | No       |
| footer_html          | string   | HTML template for the footer.                                                                                                                                                                   | No       |
| footer_url           | string   | A URL pointing to a HTML template for the footer.                                                                                                                                               | No       |
| print_background     | boolean  | Print background graphics. Defaults to false.                                                                                                                                                   | No       |
| prefer_css_page_size | boolean  | Give any CSS @page size declared in the page priority over what is declared in `width` and `height` or `format` options. Defaults to false, which will scale the content to fit the paper size. | No       |
| watermark_url        | string   | A URL pointing to a PNG or JPEG image that will be used as a watermark.                                                                                                                         | No       |
| watermark_position   | string   | A string telling where to place the watermark on the page: 'topleft', 'topright', 'center', 'bottomleft', 'bottomright'. Defaults to 'center'.                                                  | No       |
| watermark_offset_x   | number   | The number of pixels to shift the watermark image horizontally. Offset is given in pixels. Defaults to 0.                                                                                       | No       |
| watermark_offset_y   | number   | The number of pixels to shift the watermark image vertically. Offset is given in pixels. Defaults to 0.                                                                                         | No       |
| title                | string   | The title of this document.                                                                                                                                                                     | No       |
| author               | string   | The author of this document.                                                                                                                                                                    | No       |
| creator              | string   | The creator of this document.                                                                                                                                                                   | No       |
| subject              | string   | The subject of this document.                                                                                                                                                                   | No       |
| keywords             | string[] | An array of keywords associated with this document.                                                                                                                                             | No       |
| language             | string   | An RFC 3066 language-tag denoting the language of this document, or an empty string if the language is unknown.                                                                                 | No       |
| encryption           | string   | Encrypt the PDF document using one of following algorithms: 'aes-256', 'aes-128', 'rc4-128' or 'rc4-40'.                                                                                        | No       |
| owner_password       | string   | Set the owner password (required when encryption is enabled).                                                                                                                                   | No       |
| user_password        | string   | Set the user password.                                                                                                                                                                          | No       |
| permissions          | string   | Set user permissions 'all' or 'none'. Defaults to 'all'.                                                                                                                                        | No       |

> \* = You must supply either a `document_url` or a `document_html`a string containing raw HTML markup.

#### Returns

This call returns a PDF document if the request was successfully processed. An [error](/rest/#errors) is returned if something goes wrong.

<hr/>

### Creating a PDF from a URL

To create a PDF from a URL you can use the `document_url` parameter. You must specify the full URL to the page you want to convert, including the http:// or https:// prefix.
If you specify an invalid URL, or if the web page is not accessible, the request will fail.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Creating a PDF from HTML source

The other method of creating a PDF is sending your raw HTML code in the `document_html` parameter. If you send invalid HTML, the result of this request is unknown.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "{ \"document_html\": \"<html><body>Test...</body></html>\" }" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Content-Type: application/json" \
      -o file.pdf
```

<hr/>

### Layout

<div style="margin-top: 20px"></div>

#### Page format

There are several parameters you can use to specify the layout of the PDF document.
By default the PDF page size is set to `A4`. You can change the page size by using the `page_format` parameter.
This parameter can be one of the following values:

- `Executive`, `Folio`, `Ledger`, `Legal`, `Letter`, `Tabloid`
- `Envelope#10`,`EnvelopeDL`,`EnvelopeMonarch`
- `A0`,`A1`,`A2`,`A3`,`A4`,`A5`,`A6`,`A7`
- `B0`,`B1`,`B2`,`B3`,`B4`,`B5`,`B6`,`B7`
- `C0`,`C1`,`C2`,`C3`,`C4`,`C5`,`C6`,`C7`

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "page_format=A4" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### Page width and height

In stead of using `page_format` you can specify the size of the page using the `page_width` and `page_height` parameters.
Please note that these parameters will take effect over the `page_format` parameter. You can use the following units to specify width and height:

- `px` - pixel
- `in` - inch
- `cm` - centimeter
- `mm` - millimeter

The default unit is pixels.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "page_width=210mm" \
      -d "page_height=297mm" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### Scaling

To change the scale of the web page rendering you can use the `scale` parameter. The normal scale is 1, but you can specify a custom amount between 0.1 and 2, in order to reduce or enlarge the page content.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "scale=0.7" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### Page ranges

By default all pages of the PDF document are returned in the response. By using the `page_ranges` parameter, you can specify which pages you are interested in. You can specify a range, e.g. '1-5', or individuale pages like '1,3,5'.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "page_ranges=1-5,8,11-13" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### Margins

The generated PDF document has a default margin set to 20 millimeters on each side. To specify your own margins use can use the `margin_top`, `margin_left`,
`margin_bottom` and `margin_right` parameters. You can use the following units to specify margins:

- `px` - pixel
- `in` - inch
- `cm` - centimeter
- `mm` - millimeter

The default unit is pixels.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "margin_top=25mm" \
      -d "margin_left=25mm" \
      -d "margin_bottom=25mm" \
      -d "margin_right=25mm" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### Orientation

By default the page orientation is set to portrait. Using the `orientation` parameter the page orientation can be set to either `portrait` or `landscape`.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "orientation=landscape" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Headers

There are three ways of setting the page header:

1. Specify a simple text header using the `header_text` parameter.
2. Use a HTML header with the `header_html` parameter.
3. Specify a URL to a HTML template using the `header_url` parameter.

In the header you can use the following tags:

- `[page]` - the number of the current page
- `[totalpages]` - the total number of pages in thedocument
- `[date]` - the current date and time
- `[isodate]` - the current date and time in ISO 8601 format
- `[title]` - the title of the web site
- `[url]` - the url of the web site

#### Text header

By default the header text is aligned at the center of the document. You can use the `header_align` parameter to
specify the alignment, which can be one of the following: `left`, `right` or `center`.

There is no left or right margin for the header by default. However, you may set your own custom margin using the `header_margin`
parameter. The `header_margin` is given in pixels and sets both the left and right margin.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "header_text=This is the header" \
      -d "header_align=left" \
      -d "header_margin=20" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### HTML headers

The default header template looks like this:

```html
<div style="width: 100%; color: lightgray; text-align: center;">
  <span style="font-size: 9px; margin: 0 20px;">${headerText}</span>
</div>
```

If the default header does not suit your needs, you can set your header template using
the `header_html` or `header_url` parameters.

You can still use the tags like `[page]` listed above in this template.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "{ \"document_url\": \"https://example.com\", \"header_html\": \"<div>...</div>\" }" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Content-Type: application/json" \
      -o file.pdf
```

**and with a URL**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "header_url=https://example.com/header.html" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Footers

The footer parameters work the exact same way as the header parameters.

1. Specify a simple text footer using the `footer_text` parameter.
2. Use a HTML footer with the `footer_html` parameter.
3. Specify a URL to a HTML template using the `footer_url` parameter.

#### Text footer

By default the footer text is aligned at the center of the document. You can use the `footer_align` parameter to
specify the alignment, which can be one of the following: `left`, `right` or `center`.

There is no left or right margin for the footer by default. However, you may set your own custom margin using the `footer_margin`
parameter. The `footer_margin` is given in pixels and sets both the left and right margin.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "footer_text=[page] of [totalpages]" \
      -d "footer_align=center" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

#### HTML footers

The default footer template looks like this:

```html
<div style="width: 100%; color: lightgray; text-align: center;">
  <span style="font-size: 9px; margin: 0 20px;">${footerText}</span>
</div>
```

If the default footer does not suit your needs, you can set your footer template using
the `footer_html` or `footer_url` parameters.

You can still use the tags like `[page]` listed above in this template.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "{ \"document_url\": \"https://example.com\", \"footer_html\": \"<div>...</div>\" }" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Content-Type: application/json" \
      -o file.pdf
```

**and with a URL**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "footer_url=https://example.com/footer.html" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Watermarks

You can add a watermark image to each page with the `watermark_url` parameter. This URL must be a valid PNG or JPEG image, the file extension must be `.png`,
`.jpg`, or `.jpeg`. We recommend using a PNG image because they can have a transparent backgrounds, making them easier to position on the page. The watermark image is not changed in anyway before it is placed on the page. The original size and opacity is used, so we advise you to edit the watermark image before using it.

By default, the watermark image is placed at the center of the page. You man change the default position by using the `watermark_position` parameter and set the position to
`topleft`, `topright`, `bottomleft`, `bottomright`, or `center`.

To adjust the position of the watermark image to fit your page, you can use the `watermark_offset_x` and `watermark_offset_y` parameters. With these parameters you move the image horizontally and vertically, respectively. The offsets are given in pixels.

Please note that position (0,0) is in the top left corner of the page. Adobe is using the bottom left corner in the PDF specification. To move left with `watermark_offset_x` or to move up with `watermark_offset_y`, use a negative value.

::: tip
We support watermark images only, not plain text. To display text, create a PNG image with a transparent background and put some text on top of it.
:::

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "watermark_url=https://example.com/watermark.png" \
      -d "watermark_position=topleft" \
      -d "watermark_offset_x=50" \
      -d "watermark_offset_y=50" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Security

You can limit access to a PDF by setting passwords and by restricting certain features, such as printing and editing. However, you cannot prevent saving copies of a PDF. The copies have the same restrictions as the original PDF. Two types of passwords are available:

- User password requires a user to type a password to open the PDF.
- Owner password requires a password to change permission settings. Using a permissions password, you can restrict printing, editing, and copying content in the PDF. Recipients don’t need a password to open the document in Reader or Acrobat. They do need a password to change the restrictions you've set.
  If the PDF is secured with both types of passwords, it can be opened with either password. However, only the permissions password allows the user to change the restricted features. Because of the added security, setting both types of passwords is often beneficial.

When you try to open an password protected PDF document in Preview on a Mac:

<img src="/img/screenshots/pdf-encryption1.png" style="width:500px" />

<br/>

If you enter the user password and try to print the document:

<img src="/img/screenshots/pdf-encryption2.png" style="width:500px" />

<br/>

The document properties when no password is entered:

<img src="/img/screenshots/pdf-encryption3.png" style="width:300px" />

<br/>

The document properties when the user password is entered:

<img src="/img/screenshots/pdf-encryption4.png" style="width:300px" />

<br/>

The document properties when the owner password is entered:

<img src="/img/screenshots/pdf-encryption5.png" style="width:300px" />

<br/>

To protect the PDF document you may want to encrypt it and apply a password.

To protect a PDF document, use the `encryption` and `owner_password` parameters. The `encryption` parameter can be one of:

- `aes-256` (best and recommended)
- `aes-128`
- `rc4-128`
- `rc4-40` (weak, not recommended)

::: danger IMPORTANT
Your PDF reader must support PDF version 1.7 or later in order to open documents encrypted with AES 256.
:::

Optionally, you can also specify a user password using the `user_password` parameter.

By default the user has all permissions, but you can change this by using the `permissions` parameter. This parameter take either `all` or `none`.

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "encryption=aes-256" \
      -d "owner_password=pass123" \
      -d "user_password=pass234" \
      -d "permissions=none" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Meta data

When you open a PDF document in **Preview** on a Mac, use **Tools** | **Show Inspector** to view all the document properties:

<img src="/img/screenshots/pdf-info1.png" style="width:300px" />
<img src="/img/screenshots/pdf-info2.png" style="width:300px" />

To add these properties (also called meta data) to the PDF document, use one or more of the following parameters:

- `title` - the title of the document.
- `author` - the name of the document author.
- `creator` - document creator.
- `subject` - the subject or description of the document.
- `keywords` - document keyword tags (an array of words).
- `language` - language specified using a RFC 3066 language-tag, e.g. "en-us".

**Example request**

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "title=The title of the document" \
      -d "author=John Doe" \
      -d "subject=My document" \
      -d "keywords=['this','is','a','test']" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```

<hr/>

### Miscellaneous

- `document_name` - Set the file name of the document. By default 'document.pdf'
- `print_background` - Print background graphics. Defaults to `false`.
- `prefer_css_page_size` - Give any CSS @page size declared in the page priority over what is declared in width and height or format options. Defaults to `false`, which will scale the content to fit the paper size.

```bash
curl https://api.salesfly.com/v1/pdf/create \
      -d "document_url=https://example.com" \
      -d "document_name=page1.pdf" \
      -d "print_background=true" \
      -d "prefer_css_page_size=true" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -o file.pdf
```
