# Geolocation API

The Geolocation API provides real-time look up of geographical locations using IP addresses.

## Get location by IP

### <span class="get">GET</span> /v1/geoip/<span class="param">:ip</span> <span class="query">?fields=<span class="arg">string</span> &hostname=<span class="arg">boolean</span> &security=<span class="arg">boolean</span></span>

Retrieves the geographical location for a given IP address or domain name.

#### Arguments

| Argument | Type    | Description                                                                                 | Required |
| :------- | :------ | :------------------------------------------------------------------------------------------ | :------- |
| ip       | string  | The IP address or domain name to look up.                                                   | Yes      |
| fields   | string  | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| hostname | boolean | Set to true if you want to reverse lookup ip address to hostname                            | No       |
| security | boolean | Set to true if you want additional security info                                            | No       |

Use the optional query parameters to change the response data. You can specify which fields you are interested in by using ?fields=field1,field2,field3. Ie. fields=country_code, to only get the country code. This will save both time and bandwidth. You can also ask to lookup the name of the host having the specified IP address. To do this, set hostname=true. This will add extra processing to the request, making it a bit slower. Finally, you can set security=true to get more security information about the IP address. This can be used to prevent fraud and unwanted visitors.

#### Returns

This call returns an IP location object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/geoip/8.8.8.8 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "ip": "8.8.8.8",
    "type": "ipv4",
    "continent_name": "North America",
    "continent_code": "NA",
    "country_name": "United States",
    "country_name_native": "United States",
    "country_code": "US",
    "country_code3": "USA",
    "capital": "Washington D.C.",
    "region_name": "California",
    "region_code": "CA",
    "city": "Mountain View",
    "postcode": "94043",
    "latitude": 37.405992,
    "longitude": -122.078515,
    "phone_prefix": "+1",
    "currencies": [
      {
        "code": "USD",
        "num_code": "840",
        "name": "US Dollar",
        "name_plural": "US dollars",
        "symbol": "$",
        "symbol_native": "$",
        "decimal_digits": 2
      }
    ],
    "languages": [
      {
        "code": "eng",
        "code2": "en",
        "name": "English",
        "native_name": "English",
        "rtl": false
      }
    ],
    "flag": "https://cdn.salesfly.com/flags/us.svg",
    "flag_emoji": "🇺🇸",
    "is_eu": false,
    "internet_tld": ".us",
    "timezone": {
      "id": "America/Los_Angeles",
      "localtime": "2019-02-19T08:56:28-08:00",
      "gmt_offset": -28800,
      "code": "PST",
      "daylight_saving": true
    }
  },
  "status": 200,
  "success": true
}
```

### XML response:

```xml
<result>
  <status>200</status>
  <success>true</success>
  <data>
    <ip>8.8.8.8</ip>
    <type>ipv4</type>
    <continent_name>North America</continent_name>
    <continent_code>NA</continent_code>
    <country_name>United States</country_name>
    <country_name_native>United States</country_name_native>
    <country_code>US</country_code>
    <country_code3>USA</country_code3>
    <capital>Washington D.C.</capital>
    <region_name>California</region_name>
    <region_code>CA</region_code>
    <city>Mountain View</city>
    <postcode>94043</postcode>
    <latitude>37.405992</latitude>
    <longitude>-122.078515</longitude>
    <phone_prefix>+1</phone_prefix>
    <currencies>
      <code>USD</code>
      <num_code>840</num_code>
      <name>US Dollar</name>
      <plural_name>US dollars</plural_name>
      <symbol>$</symbol>
      <native_symbol>$</native_symbol>
      <decimal_digits>2</decimal_digits>
    </currencies>
    <languages>
      <code>eng</code>
      <code2>en</code2>
      <name>English</name>
      <native_name>English</native_name>
      <rtl>false</rtl>
    </languages>
    <flag>https://cdn.salesfly.com/flags/us.svg</flag>
    <flag_emoji>🇺🇸</flag_emoji>
    <is_eu>false</is_eu>
    <internet_tld>.us</internet_tld>
    <timezone>
      <id>America/Los_Angeles</id>
      <localtime>2019-02-19T08:57:13-08:00</localtime>
      <gmt_offset>-28800</gmt_offset>
      <code>PST</code>
      <daylight_saving>true</daylight_saving>
    </timezone>
  </data>
</result>
```

### Example request with options

```bash
curl https://api.salesfly.com/v1/geoip/8.8.8.8?fields=country_code,hostname&hostname=true \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "hostname": "google-public-dns-a.google.com",
    "country_code": "US"
  },
  "status": 200,
  "success": true
}
```

## Get location of multiple IP addresses

### <span class="get">GET</span> /v1/geoip/<span class="param">:iplist</span> <span class="query">?fields=<span class="arg">string</span> &hostname=<span class="arg">boolean</span> &security=<span class="arg">boolean</span></span>

Retrieves the geographical location for a given IP addresses or domain names.

#### Arguments

| Argument | Type    | Description                                                                                 | Required |
| :------- | :------ | :------------------------------------------------------------------------------------------ | :------- |
| iplist   | string  | A comma separated string with the IP addresses or domain names to look up.                  | Yes      |
| fields   | string  | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| hostname | boolean | Set to true if you want to reverse lookup ip address to hostname                            | No       |
| security | boolean | Set to true if you want additional security info                                            | No       |

#### Returns

This call returns one or more IP location objects, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/geoip/1.1.1.1,2.2.2.2,3.3.3.3 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": [
    {
      "ip": "1.1.1.1",
      "type": "ipv4",
      "continent_name": "Oceania",
      "continent_code": "OC",
      "country_name": "Australia",
      "country_name_native": "Australia",
      "country_code": "AU",
      "country_code3": "AUS",
      "capital": "Canberra",
      "region_name": "Queensland",
      "region_code": "QLD",
      "city": "Brisbane",
      "postcode": "4000",
      "latitude": -27.46794,
      "longitude": 153.02809,
      "phone_prefix": "+61",
      "currencies": [
        {
          "code": "AUD",
          "num_code": "036",
          "name": "Australian Dollar",
          "name_plural": "Australian dollars",
          "symbol": "A$",
          "symbol_native": "$",
          "decimal_digits": 2
        }
      ],
      "languages": [
        {
          "code": "eng",
          "code2": "en",
          "name": "English",
          "native_name": "English",
          "rtl": false
        }
      ],
      "flag": "https://cdn.salesfly.com/flags/au.svg",
      "flag_emoji": "🇦🇺",
      "is_eu": false,
      "internet_tld": ".au",
      "timezone": {
        "id": "Australia/Brisbane",
        "localtime": "2019-04-29T17:14:42+10:00",
        "gmt_offset": 36000,
        "code": "AEST",
        "daylight_saving": false
      }
    },
    {
      "ip": "2.2.2.2",
      "type": "ipv4",
      "continent_name": "Europe",
      "continent_code": "EU",
      "country_name": "France",
      "country_name_native": "France",
      "country_code": "FR",
      "country_code3": "FRA",
      "capital": "Paris",
      "region_name": "Ile-de-France",
      "region_code": "J",
      "city": "Issy-les-Moulineaux",
      "postcode": "92867",
      "latitude": 48.82104,
      "longitude": 2.27718,
      "phone_prefix": "+33",
      "currencies": [
        {
          "code": "EUR",
          "num_code": "978",
          "name": "Euro",
          "name_plural": "Euros",
          "symbol": "€",
          "symbol_native": "€",
          "decimal_digits": 2
        }
      ],
      "languages": [
        {
          "code": "fra",
          "code2": "fr",
          "name": "French",
          "native_name": "Français",
          "rtl": false
        }
      ],
      "flag": "https://cdn.salesfly.com/flags/fr.svg",
      "flag_emoji": "🇫🇷",
      "is_eu": true,
      "internet_tld": ".fr",
      "timezone": {
        "id": "Europe/Paris",
        "localtime": "2019-04-29T09:14:42+02:00",
        "gmt_offset": 7200,
        "code": "CEST",
        "daylight_saving": true
      }
    },
    {
      "ip": "3.3.3.3",
      "type": "ipv4",
      "continent_name": "North America",
      "continent_code": "NA",
      "country_name": "United States",
      "country_name_native": "United States",
      "country_code": "US",
      "country_code3": "USA",
      "capital": "Washington D.C.",
      "region_name": "Washington",
      "region_code": "WA",
      "city": "Seattle",
      "postcode": "98109",
      "latitude": 47.6275,
      "longitude": -122.3462,
      "phone_prefix": "+1",
      "currencies": [
        {
          "code": "USD",
          "num_code": "840",
          "name": "US Dollar",
          "name_plural": "US dollars",
          "symbol": "$",
          "symbol_native": "$",
          "decimal_digits": 2
        }
      ],
      "languages": [
        {
          "code": "eng",
          "code2": "en",
          "name": "English",
          "native_name": "English",
          "rtl": false
        }
      ],
      "flag": "https://cdn.salesfly.com/flags/us.svg",
      "flag_emoji": "🇺🇸",
      "is_eu": false,
      "internet_tld": ".us",
      "timezone": {
        "id": "America/Los_Angeles",
        "localtime": "2019-04-29T00:14:42-07:00",
        "gmt_offset": -25200,
        "code": "PDT",
        "daylight_saving": true
      }
    }
  ],
  "status": 200,
  "success": true
}
```

### XML response

```xml
<result>
    <status>200</status>
    <success>true</success>
    <dataset>
        <data index="0">
            <ip>1.1.1.1</ip>
            <type>ipv4</type>
            <continent_name>Oceania</continent_name>
            <continent_code>OC</continent_code>
            <country_name>Australia</country_name>
            <country_name_native>Australia</country_name_native>
            <country_code>AU</country_code>
            <country_code3>AUS</country_code3>
            <capital>Canberra</capital>
            <region_name>Queensland</region_name>
            <region_code>QLD</region_code>
            <city>Brisbane</city>
            <postcode>4000</postcode>
            <latitude>-27.46794</latitude>
            <longitude>153.02809</longitude>
            <phone_prefix>+61</phone_prefix>
            <currencies>
                <code>AUD</code>
                <num_code>036</num_code>
                <name>Australian Dollar</name>
                <plural_name>Australian dollars</plural_name>
                <symbol>A$</symbol>
                <native_symbol>$</native_symbol>
                <decimal_digits>2</decimal_digits>
            </currencies>
            <languages>
                <code>eng</code>
                <code2>en</code2>
                <name>English</name>
                <native_name>English</native_name>
                <rtl>false</rtl>
            </languages>
            <flag>https://cdn.salesfly.com/flags/au.svg</flag>
            <flag_emoji>🇦🇺</flag_emoji>
            <is_eu>false</is_eu>
            <internet_tld>.au</internet_tld>
            <timezone>
                <id>Australia/Brisbane</id>
                <localtime>2019-04-29T17:15:44+10:00</localtime>
                <gmt_offset>36000</gmt_offset>
                <code>AEST</code>
                <daylight_saving>false</daylight_saving>
            </timezone>
        </data>
        <data index="1">
            <ip>2.2.2.2</ip>
            <type>ipv4</type>
            <continent_name>Europe</continent_name>
            <continent_code>EU</continent_code>
            <country_name>France</country_name>
            <country_name_native>France</country_name_native>
            <country_code>FR</country_code>
            <country_code3>FRA</country_code3>
            <capital>Paris</capital>
            <region_name>Ile-de-France</region_name>
            <region_code>J</region_code>
            <city>Issy-les-Moulineaux</city>
            <postcode>92867</postcode>
            <latitude>48.82104</latitude>
            <longitude>2.27718</longitude>
            <phone_prefix>+33</phone_prefix>
            <currencies>
                <code>EUR</code>
                <num_code>978</num_code>
                <name>Euro</name>
                <plural_name>Euros</plural_name>
                <symbol>€</symbol>
                <native_symbol>€</native_symbol>
                <decimal_digits>2</decimal_digits>
            </currencies>
            <languages>
                <code>fra</code>
                <code2>fr</code2>
                <name>French</name>
                <native_name>Français</native_name>
                <rtl>false</rtl>
            </languages>
            <flag>https://cdn.salesfly.com/flags/fr.svg</flag>
            <flag_emoji>🇫🇷</flag_emoji>
            <is_eu>true</is_eu>
            <internet_tld>.fr</internet_tld>
            <timezone>
                <id>Europe/Paris</id>
                <localtime>2019-04-29T09:15:44+02:00</localtime>
                <gmt_offset>7200</gmt_offset>
                <code>CEST</code>
                <daylight_saving>true</daylight_saving>
            </timezone>
        </data>
        <data index="2">
            <ip>3.3.3.3</ip>
            <type>ipv4</type>
            <continent_name>North America</continent_name>
            <continent_code>NA</continent_code>
            <country_name>United States</country_name>
            <country_name_native>United States</country_name_native>
            <country_code>US</country_code>
            <country_code3>USA</country_code3>
            <capital>Washington D.C.</capital>
            <region_name>Washington</region_name>
            <region_code>WA</region_code>
            <city>Seattle</city>
            <postcode>98109</postcode>
            <latitude>47.6275</latitude>
            <longitude>-122.3462</longitude>
            <phone_prefix>+1</phone_prefix>
            <currencies>
                <code>USD</code>
                <num_code>840</num_code>
                <name>US Dollar</name>
                <plural_name>US dollars</plural_name>
                <symbol>$</symbol>
                <native_symbol>$</native_symbol>
                <decimal_digits>2</decimal_digits>
            </currencies>
            <languages>
                <code>eng</code>
                <code2>en</code2>
                <name>English</name>
                <native_name>English</native_name>
                <rtl>false</rtl>
            </languages>
            <flag>https://cdn.salesfly.com/flags/us.svg</flag>
            <flag_emoji>🇺🇸</flag_emoji>
            <is_eu>false</is_eu>
            <internet_tld>.us</internet_tld>
            <timezone>
                <id>America/Los_Angeles</id>
                <localtime>2019-04-29T00:15:44-07:00</localtime>
                <gmt_offset>-25200</gmt_offset>
                <code>PDT</code>
                <daylight_saving>true</daylight_saving>
            </timezone>
        </data>
    </dataset>
</result>
```

## Get location of caller

### <span class="get">GET</span> /v1/geoip/myip <span class="query">?fields=<span class="arg">string</span> &hostname=<span class="arg">boolean</span> &security=<span class="arg">boolean</span></span>

Retrieves the geographical location for caller's IP address.

#### Arguments

| Argument | Type    | Description                                                                                 | Required |
| :------- | :------ | :------------------------------------------------------------------------------------------ | :------- |
| fields   | string  | A comma separated list of fields to display (snake_case), rest of fields will be 0 or empty | No       |
| hostname | boolean | Set to true if you want to reverse lookup ip address to hostname                            | No       |
| security | boolean | Set to true if you want additional security info                                            | No       |

#### Returns

This call returns an IP location object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/geoip/myip \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```
