# Products API

You can use the Products API to access and manipulate a store's catalog.

## Get product

### <span class="get">GET</span> /v1/products/<span class="param">:id</span>

Retrieves the product for the given id.

#### Arguments

| Argument | Type   | Description                   | Required |
| :------- | :----- | :---------------------------- | :------- |
| id       | string | ID of the product to look up. | Yes      |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/products/1 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "id": 1,
    "name": "Widget",
    "description": "The widget is a test product",
    "price": 9.99
  },
  "status": 200,
  "success": true
}
```

## Add product

### <span class="post">POST</span> /v1/products

Adds a new product to your catalog.

#### Arguments

| Argument    | Type   | Description          | Required |
| :---------- | :----- | :------------------- | :------- |
| name        | string | Product name.        | Yes      |
| description | string | Product description. | Yes      |
| price       | number | Product price        | Yes      |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/products/1 \
      -d "name=Widget" \
      -d "description=The widget is a test product" \
      -d "price=9.99" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "id": "1",
    "name": "Widget",
    "description": "The widget is a test product",
    "price": 9.99
  },
  "status": 200,
  "success": true
}
```

## Update product

### <span class="put">PUT</span> /v1/products/<span class="param">:id</span>

Updates the product for the given id.

#### Arguments

| Argument    | Type   | Description                       | Required |
| :---------- | :----- | :-------------------------------- | :------- |
| id          | string | ID of the product to look update. | Yes      |
| name        | string | Product name.                     | No       |
| description | string | Product description.              | No       |
| price       | number | Product price                     | No       |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/products/1 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "id": "1",
    "name": "Widget",
    "description": "The widget is a test product",
    "price": 9.99
  },
  "status": 200,
  "success": true
}
```

## Remove product

### <span class="delete">DELETE</span> /v1/products/<span class="param">:id</span>

Deletes the product for the given id.

#### Arguments

| Argument | Type   | Description                  | Required |
| :------- | :----- | :--------------------------- | :------- |
| id       | string | ID of the product to delete. | Yes      |

#### Returns

This call returns a product object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl -X DELETE https://api.salesfly.com/v1/products/1 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "status": 200,
  "success": true
}
```
