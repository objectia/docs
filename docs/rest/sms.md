# SMS API

The SMS API is an easy way to send SMS text messages from your web site, app or any other kind of software. You can use this API to send a message to almost any mobile
phone in the world.

## Send text message

### <span class="post">POST</span> /v1/sms/send

This endpoint sends a text message to a recipient.

#### Arguments

A message object can contain the following attributes:

| Attribute | Type   | Description                                                                           | Required |
| :-------- | :----- | :------------------------------------------------------------------------------------ | :------- |
| from      | string | The name the message should be sent from (sender ID).                                 | Yes      |
| to        | string | The number that the message should be sent to. Numbers are specified in E.164 format. | Yes      |
| text      | string | The body of the message being sent.                                                   | Yes      |

> Please note that alphanumeric sender IDs are not supported in all countries. In these cases sender phone number will be used.

#### Returns

This call returns a message receipt if the message was successfully processed. An [error](/rest/#errors) is returned if something goes wrong.

### JSON request

```bash
curl https://api.salesfly.com/v1/sms/send \
      -d '{"from":"Myself","to":"+12125551234","text":"Hello world!"}' \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json" \
      -H "Content-Type: application/json"
```

Response:

```json
{
  "data": {
    "id": "15000000BDDAE964",
    "from": "Myself",
    "text": "Hello world!",
    "to": "+12125551234",
    "price": 0.0072
  },
  "status": 200,
  "success": true
}
```
