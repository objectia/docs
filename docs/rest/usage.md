# Usage API

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption across products for your account.
It is the most important and best tool for helping to monitor and manage usage across the different APIs under your account.

## Get usage data

### <span class="get">GET</span> /v1/usage

Retrieves the usage data for the current month. The data section contains number of requests for each service.

#### Arguments

None

#### Returns

This call returns an API usage object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/usage \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "allowed": 1000,
    "used": 123
  },
  "status": 200,
  "success": true
}
```

### XML response:

```xml
<result>
  <data>
    <allowed>1000</geoip_requests>
    <used>123</mail_requests>
  </data>
  <status>200</status>
  <success>true</success>
</result>
```
