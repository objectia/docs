# Currency API

The Currency API provides currency conversion and currency rates.

Currencies are labelled using ISO 4217 3-letter codes, for example `USD` for United States Dollar and
`EUR` for Euros. By default all currencies are reported relative to USD, the base currency. However, you can
specify the base currency for most API calls.

At the moment we are supporting over 170 currencies.

## Get latest rates

### <span class="get">GET</span> /v1/currency/latest <span class="query">?base=<span class="arg">currency code</span> &currencies=<span class="arg">list of currencies</span></span>

Retrieves the latest currency rates.

#### Arguments

| Argument   | Type   | Description                                                                                  | Required |
| :--------- | :----- | :------------------------------------------------------------------------------------------- | :------- |
| base       | string | The base currency. Default base currency is USD.                                             | No       |
| currencies | string | A comma separated list of currency codes to display. By default all currencies are returned. | No       |

::: tip
Use <b>&format=xml</b> to return XML data in stead of JSON.
:::

#### Returns

This call returns a currency rate object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/latest?base=EUR&currencies=EUR,GBP,USD \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON Response

```json
{
  "data": {
    "base": "EUR",
    "date": "2020-03-16",
    "timestamp": 1584353746,
    "rates": {
      "EUR": 1,
      "GBP": 0.898809,
      "USD": 1.119358
    }
  },
  "status": 200,
  "success": true
}
```

### XML response:

```xml
<result>
    <status>200</status>
    <success>true</success>
    <data>
        <base>EUR</base>
        <date>2020-03-16</date>
        <timestamp>1584353746</timestamp>
        <rates>
            <rate code="EUR">1</rate>
            <rate code="GBP">0.898809</rate>
            <rate code="USD">1.119358</rate>
        </rates>
    </data>
</result>
```

## Get historical rates

### <span class="get">GET</span> /v1/currency/historical/<span class="param">:date</span><span class="query"> ?base=<span class="arg">currency code</span> &currencies=<span class="arg">list of currencies</span></span>

Retrieves the currency rates for a given date. We have historical data back to January 1, 2000.

#### Arguments

| Argument   | Type   | Description                                                                                  | Required |
| :--------- | :----- | :------------------------------------------------------------------------------------------- | :------- |
| date       | date   | The date to look up. Use the following format: YYYY-MM-DD                                    | Yes      |
| base       | string | The base currency. Default base currency is USD.                                             | No       |
| currencies | string | A comma separated list of currency codes to display. By default all currencies are returned. | No       |

#### Returns

This call returns a currency rate object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/2020-3-13?base=EUR&currencies=EUR,GBP,USD \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": {
    "base": "EUR",
    "date": "2020-03-13",
    "timestamp": 1584118800,
    "rates": {
      "EUR": 1,
      "GBP": 0.892167,
      "USD": 1.107
    }
  },
  "status": 200,
  "success": true
}
```

### XML response

```xml
<result>
    <status>200</status>
    <success>true</success>
    <data>
        <base>EUR</base>
        <date>2020-03-13</date>
        <timestamp>1584118800</timestamp>
        <rates>
            <rate code="EUR">1</rate>
            <rate code="GBP">0.892167</rate>
            <rate code="USD">1.107</rate>
        </rates>
    </data>
</result>
```

## Convert currency

### <span class="get">GET</span> /v1/currency/convert/<span class="param">:amount</span>/<span class="param">:from</span>/<span class="param">:to</span> <span class="query"> ?date=<span class="arg">date</span></span>

Converts an amount from one currency to another.

#### Arguments

| Argument | Type   | Description                                                                             | Required |
| :------- | :----- | :-------------------------------------------------------------------------------------- | :------- |
| amount   | number | The amount to convert, for example 8.99                                                 | Yes      |
| from     | string | The code of the currency to convert from                                                | Yes      |
| to       | string | The code of the currency to convert to                                                  | Yes      |
| date     | date   | An optional date to use in stead of currenct date. Use the following format: YYYY-MM-DD | No       |

#### Returns

This call returns a currency exchange object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/convert/8.99/USD/EUR \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": {
    "amount": 8.99,
    "from": "USD",
    "to": "EUR",
    "date": "2020-03-16",
    "timestamp": 1584353746,
    "rate": 0.893369,
    "result": 8.031387
  },
  "status": 200,
  "success": true
}
```

## Get currency rate changes

### <span class="get">GET</span> /v1/currency/change/<span class="param">:start</span>/<span class="param">:end</span> <span class="query"> ?base=<span class="arg">currency code</span> &currencies=<span class="arg">list of currencies</span></span>

Retrieves the currency rate changes for the specified currencies.

#### Arguments

| Argument   | Type   | Description                                                                                  | Required |
| :--------- | :----- | :------------------------------------------------------------------------------------------- | :------- |
| start      | date   | The start date of the period                                                                 | Yes      |
| end        | date   | The end date of the period                                                                   | Yes      |
| base       | string | The base currency. Default base currency is USD.                                             | No       |
| currencies | string | A comma separated list of currency codes to display. By default all currencies are returned. | No       |

#### Returns

This call returns a currency change object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/change/2020-03-13/2020-03-16?currencies=EUR,GBP,USD \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": {
    "base": "USD",
    "start_date": "2020-03-13",
    "end_date": "2020-03-16",
    "rates": {
      "EUR": {
        "start": 0.903342,
        "end": 0.893369,
        "change": -0.009973,
        "change_percent": -1.104012
      },
      "GBP": {
        "start": 0.805932,
        "end": 0.802968,
        "change": -0.002964,
        "change_percent": -0.367773
      },
      "USD": {
        "start": 1,
        "end": 1
      }
    }
  },
  "status": 200,
  "success": true
}
```

## Get currency timeframe

### <span class="get">GET</span> /v1/currency/timeframe/<span class="param">:currency</span>/<span class="param">:start</span>/<span class="param">:end</span> <span class="query"> ?base=<span class="arg">currency code</span> </span>

Retrieves the currency rate changes for the specified currencies.

#### Arguments

| Argument | Type   | Description                                      | Required |
| :------- | :----- | :----------------------------------------------- | :------- |
| currency | string | The code of the currency to be displayed         | Yes      |
| start    | date   | The start date of the period                     | Yes      |
| end      | date   | The end date of the period                       | Yes      |
| base     | string | The base currency. Default base currency is USD. | No       |

> NOTE: Max timespan is 365 days.

#### Returns

This call returns a currency timeframe object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/timeframe/EUR/2020-03-13/2020-03-16 \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": {
    "base": "USD",
    "currency": "EUR",
    "start_date": "2020-03-13",
    "end_date": "2020-03-16",
    "timespan": 4,
    "rates": {
      "2020-03-13": 0.903342,
      "2020-03-14": 0.895587,
      "2020-03-15": 0.893823,
      "2020-03-16": 0.893369
    }
  },
  "status": 200,
  "success": true
}
```

## List supported currencies

### <span class="get">GET</span> /v1/currency/list

Retrieves the list of supported currencies.

#### Arguments

None

#### Returns

This call returns a supported currency object, and returns an [error](/rest/#errors) otherwise.

### Example request

```bash
curl https://api.salesfly.com/v1/currency/list \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json"
```

### JSON response

```json
{
  "data": {
    "currencies": {
      "AED": "United Arab Emirates Dirham",
      "AFN": "Afghan Afghani",
      "ALL": "Albanian Lek",
      "AMD": "Armenian Dram",
      "ANG": "Netherlands Antillean Guilder",
      "AOA": "Angolan Kwanza",
      "ARS": "Argentine Peso",
      "AUD": "Australian Dollar",
      "AWG": "Aruban Florin",
      "AZN": "Azerbaijani Manat",
      "BAM": "Bosnia-Herzegovina Convertible Mark",
      "BBD": "Barbadian Dollar",
      "BDT": "Bangladeshi Taka",
      "BGN": "Bulgarian Lev",
      "BHD": "Bahraini Dinar",
      "BIF": "Burundian Franc",
      "BMD": "Bermudan Dollar",
      "BND": "Brunei Dollar",
      "BOB": "Bolivian Boliviano",
      "BRL": "Brazilian Real",
      "BSD": "Bahamian Dollar",
      "BTN": "Bhutanese Ngultrum",
      "BWP": "Botswanan Pula",
      "BYN": "Belarusian Ruble",
      "BZD": "Belize Dollar",
      "CAD": "Canadian Dollar",
      "CDF": "Congolese Franc",
      "CHF": "Swiss Franc",
      "CKD": "Cook Islands Dollar",
      "CLF": "Chilean Unit of Account (UF)",
      "CLP": "Chilean Peso",
      "CNH": "Chinese Yuan (Offshore)",
      "CNY": "Chinese Yuan",
      "COP": "Colombian Peso",
      "CRC": "Costa Rican Colón",
      "CUC": "Cuban Convertible Peso",
      "CUP": "Cuban Peso",
      "CVE": "Cape Verdean Escudo",
      "CZK": "Czech Republic Koruna",
      "DJF": "Djiboutian Franc",
      "DKK": "Danish Krone",
      "DOP": "Dominican Peso",
      "DZD": "Algerian Dinar",
      "EGP": "Egyptian Pound",
      "ERN": "Eritrean Nakfa",
      "ETB": "Ethiopian Birr",
      "EUR": "Euro",
      "FJD": "Fijian Dollar",
      "FKP": "Falkland Islands Pound",
      "FOK": "Faroese Króna",
      "GBP": "British Pound Sterling",
      "GEL": "Georgian Lari",
      "GGP": "Guernsey Pound",
      "GHS": "Ghanaian Cedi",
      "GIP": "Gibraltar Pound",
      "GMD": "Gambian Dalasi",
      "GNF": "Guinean Franc",
      "GTQ": "Guatemalan Quetzal",
      "GYD": "Guyanaese Dollar",
      "HKD": "Hong Kong Dollar",
      "HNL": "Honduran Lempira",
      "HRK": "Croatian Kuna",
      "HTG": "Haitian Gourde",
      "HUF": "Hungarian Forint",
      "IDR": "Indonesian Rupiah",
      "ILS": "Israeli New Sheqel",
      "IMP": "Isle of Man Pound",
      "INR": "Indian Rupee",
      "IQD": "Iraqi Dinar",
      "IRR": "Iranian Rial",
      "ISK": "Icelandic Króna",
      "JEP": "Jersey Pound",
      "JMD": "Jamaican Dollar",
      "JOD": "Jordanian Dinar",
      "JPY": "Japanese Yen",
      "KES": "Kenyan Shilling",
      "KGS": "Kyrgystani Som",
      "KHR": "Cambodian Riel",
      "KID": "Kiribati Dollar",
      "KMF": "Comorian Franc",
      "KPW": "North Korean Won",
      "KRW": "South Korean Won",
      "KWD": "Kuwaiti Dinar",
      "KYD": "Cayman Islands Dollar",
      "KZT": "Kazakhstani Tenge",
      "LAK": "Laotian Kip",
      "LBP": "Lebanese Pound",
      "LKR": "Sri Lankan Rupee",
      "LRD": "Liberian Dollar",
      "LSL": "Lesotho Loti",
      "LYD": "Libyan Dinar",
      "MAD": "Moroccan Dirham",
      "MDL": "Moldovan Leu",
      "MGA": "Malagasy Ariary",
      "MKD": "Macedonian Denar",
      "MMK": "Myanma Kyat",
      "MNT": "Mongolian Tugrik",
      "MOP": "Macanese Pataca",
      "MRO": "Mauritanian Ouguiya (pre-2018)",
      "MRU": "Mauritanian Ouguiya",
      "MUR": "Mauritian Rupee",
      "MVR": "Maldivian Rufiyaa",
      "MWK": "Malawian Kwacha",
      "MXN": "Mexican Peso",
      "MYR": "Malaysian Ringgit",
      "MZN": "Mozambican Metical",
      "NAD": "Namibian Dollar",
      "NGN": "Nigerian Naira",
      "NIO": "Nicaraguan Córdoba",
      "NOK": "Norwegian Krone",
      "NPR": "Nepalese Rupee",
      "NZD": "New Zealand Dollar",
      "OMR": "Omani Rial",
      "PAB": "Panamanian Balboa",
      "PEN": "Peruvian Nuevo Sol",
      "PGK": "Papua New Guinean Kina",
      "PHP": "Philippine Peso",
      "PKR": "Pakistani Rupee",
      "PLN": "Polish Zloty",
      "PYG": "Paraguayan Guarani",
      "QAR": "Qatari Rial",
      "RON": "Romanian Leu",
      "RSD": "Serbian Dinar",
      "RUB": "Russian Ruble",
      "RWF": "Rwandan Franc",
      "SAR": "Saudi Riyal",
      "SBD": "Solomon Islands Dollar",
      "SCR": "Seychellois Rupee",
      "SDG": "Sudanese Pound",
      "SEK": "Swedish Krona",
      "SGD": "Singapore Dollar",
      "SHP": "Saint Helena Pound",
      "SLL": "Sierra Leonean Leone",
      "SOS": "Somali Shilling",
      "SRD": "Surinamese Dollar",
      "SSP": "South Sudanese Pound",
      "STD": "São Tomé and Príncipe Dobra (pre-2018)",
      "STN": "São Tomé and Príncipe Dobra",
      "SVC": "Salvadoran Colón",
      "SYP": "Syrian Pound",
      "SZL": "Swazi Lilangeni",
      "THB": "Thai Baht",
      "TJS": "Tajikistani Somoni",
      "TMT": "Turkmenistani Manat",
      "TND": "Tunisian Dinar",
      "TOP": "Tongan Pa'anga",
      "TRY": "Turkish Lira",
      "TTD": "Trinidad and Tobago Dollar",
      "TVD": "Tuvaluan Dollar",
      "TWD": "New Taiwan Dollar",
      "TZS": "Tanzanian Shilling",
      "UAH": "Ukrainian Hryvnia",
      "UGX": "Ugandan Shilling",
      "USD": "United States Dollar",
      "UYU": "Uruguayan Peso",
      "UZS": "Uzbekistan Som",
      "VEF": "Venezuelan Bolívar Fuerte (Old)",
      "VES": "Venezuelan Bolívar Soberano",
      "VND": "Vietnamese Dong",
      "VUV": "Vanuatu Vatu",
      "WST": "Samoan Tala",
      "XAF": "Central African CFA Franc BEAC",
      "XAG": "Silver Ounce",
      "XAU": "Gold Ounce",
      "XBT": "Bitcoin",
      "XCD": "East Caribbean Dollar",
      "XDR": "IMF Special Drawing Rights",
      "XOF": "West African CFA Franc BCEAO",
      "XPD": "Palladium Ounce",
      "XPF": "Central Pacific CFP Franc",
      "XPT": "Platinum Ounce",
      "YER": "Yemeni Rial",
      "ZAR": "South African Rand",
      "ZMW": "Zambian Kwacha"
    }
  },
  "status": 200,
  "success": true
}
```
