# Mail API

The Mail API is an easy way to send transactional mail messages from your web site, app or any other kind of software. This RESTful API hides all the complicated details of sending
SMTP mail.

The built-in tracking mechanisms makes it a powerful tool, so it is easy to see which messages that were successfully delivered and which messages that failed to reach their destinations. Detailed logs makes it
simple to diagnose and correct mail sending issues.

All email messages submitted to this API are scanned to prevent sending unwanted content. Messages flagged as either being spam or mailware will be rejected.

However, before you can send any mail, you have to add and verify your sending domains. It is mandatory to set up DNS records for SPF, DKIM and DMARC for each domain. When you add a domain like `example.come` you are only
allowed to send messages from addresses like `joe@example.com` from this domain.

## Send mail message

### <span class="post">POST</span> /v1/mail/send

This endpoint sends a mail message to one or more recipients.

> Please note that you need to submit your message as `multipart/form-data` if it has any attachments. For simple messages you can also use JSON, XML and form url encoded.

#### Arguments

A message object can contain the following attributes:

| Attribute               | Type    | Description                                                                                                | Required |
| :---------------------- | :------ | :--------------------------------------------------------------------------------------------------------- | :------- |
| from                    | string  | The email address of the sender                                                                            | Yes      |
| to                      | string  | A list of recipient email addresses                                                                        | Yes      |
| subject                 | string  | The message subject                                                                                        | Yes      |
| text                    | string  | The plain text message text                                                                                | Yes      |
| html                    | string  | The HTML message text                                                                                      |
| from_name               | string  | The name of sender                                                                                         |
| date                    | date    | The time when message was sent                                                                             |
| reply_to                | string  | The email address for replies                                                                              |
| cc                      | string  | A list of cc email addresses                                                                               |
| bcc                     | string  | A list of bcc email addresses                                                                              |
| attachments             | string  | A list of file attachments (full path)                                                                     |
| tags                    | string  | A list of tags                                                                                             |
| charset                 | string  | The charset (default: "UTF-8")                                                                             |
| encoding                | string  | The encoding (default: "quoted-printable") <br/>Other values: "base64", "8bit"                             |
| require_tls \*          | boolean | Whether to require TLS when connecting to the destination host                                             |
| verify_cert \*          | boolean | Whether to verify the SSL certificate of the remote host when connecting                                   |
| open_tracking \*        | boolean | A flag that enables or disables open tracking                                                              |
| click_tracking \*       | boolean | A flag that enables or disables click tracking (html message only)                                         |
| text_click_tracking \*  | boolean | A flag that enables or disables click tracking in plain text message (when click_tracking is also enabled) |
| unsubscribe_tracking \* | boolean | A flag that specifies if an unsubscribe link should be added                                               |
| test_mode \*            | boolean | A flag that enables test mode (email is not sent)                                                          |

> \* = Using these fields will override the default settings for the domain.

#### Returns

This call returns a message receipt if the message was successfully processed. The receipt object contains the number of accepted and rejected
recipients in addition to the message receipt ID. An [error](/rest/#errors) is returned if something goes wrong.

There are multiple sources of errors for this call:

- Missing required fields
- Trying to send from the wrong domain (all domains must be verified).
- Using invalid email addresses.
- Invalid content (flagged as spam or malware)
- Transmission errors: invalid or suppressed recipients, etc.

### JSON request

```bash
curl https://api.salesfly.com/v1/mail/send \
      -d '{"from":"me@example.com","to":["you@example.com"],"subject":"Hi","text":"This is me!"}' \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json" \
      -H "Content-Type: application/json"
```

Response:

```json
{
  "data": {
    "id": "N9AdD4YTQvKQ34",
    "accepted_recipients": 1,
    "rejected_recipients": 0
  },
  "status": 200,
  "success": true
}
```

### Form URL encoded request:

```bash
curl https://api.salesfly.com/v1/mail/send \
      -d "from=me@example.com" \
      -d "to=you@example.com" \
      -d "subject=Hi" \
      -d "text=This is me!" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/xml" \
      -H "Content-Type: application/x-www-form-urlencoded"
```

Response:

```xml
<result>
    <status>200</status>
    <success>true</success>
    <data>
        <id>RqxX2BASyGwv97</id>
        <accepted_recipients>1</accepted_recipients>
        <rejected_recipients>0</rejected_recipients>
    </data>
</result>
```

### Request with attachments (multipart/form-data)

If you want to send file attachments with your mail message you need to set content type to "multipart/form-data" in stead of JSON.

```bash
curl https://api.salesfly.com/v1/mail/send \
      -F "from=me@example.com" -F "to=you@example.com" -F "subject=Hi" -F "text=This is me!" \
      -F "me.jpg=@/path/to/pictures/me.jpg" \
      -H "Authorization: Bearer YOUR-API-KEY" \
      -H "Accept: application/json" \
      -H "Content-Type: multipart/form-data"
```

When using cURL you must add a -F option for every `field=value` you want to send.

POSTing a file with cURL is slightly different in that you need to add an @ before the file location, after the field name. The format should be:

`basename=@full-path-to-file`

where `me.jpg` is the basename of `/path/to/pictures/me.jpg` and `me.jpg` will be the file name displayed in the mail message.

Some fields expect an array, for example the `to` field. If you want to send to multiple recipients you have to add a -F option for each.

```bash
-F "to=huey@example.com" -F "to=dewey@example.com" -F "to=louie@example.com"
```
